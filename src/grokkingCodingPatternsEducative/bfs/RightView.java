package grokkingCodingPatternsEducative.bfs;

import common.*;
import java.util.*;

public class RightView {
    /**
     * Given a binary tree, return an array containing nodes in its right view. 
     * The right view of a binary tree is the set of nodes visible when the tree is seen from the right side.
     */
    public List<Integer> rightView(BinaryTreeNode<Integer> root) {
        List<Integer> results = new ArrayList<>();

        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while (!queue.isEmpty()) {
            int lastElement = 0, cnt = 0;
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode<Integer> curr = queue.poll();
                lastElement = curr.value;
                if (curr.left != null) {
                    queue.offer(curr.left);
                    cnt++;
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                    cnt++;
                }
            }
            levelCnt = cnt;
            results.add(lastElement);
        }

        return results;
    }

    public static void main(String... args) {
        RightView unit = new RightView();
        System.out.println(unit.rightView(new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4, null, null), new BinaryTreeNode<>(5, null, null)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(6, null, null), new BinaryTreeNode<>(7, null, null)))));
    }
}
