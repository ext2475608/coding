package grokkingCodingPatternsEducative.bfs;

import common.BinaryTreeNode;
import java.util.*;

public class ReverseLevelOrderTraversal {
    /**
     * Given a binary tree, populate an array to represent its level-by-level traversal in reverse order, 
     * i.e., the lowest level comes first. You should populate the values of all nodes in each level from 
     * left to right in separate sub-arrays.
     */
    public List<List<Integer>> reverseTraversal(BinaryTreeNode<Integer> root) {
        List<List<Integer>> results = new ArrayList<>();
        
        if (root == null) {
            return results;
        }

        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while(!queue.isEmpty()) {
            int cnt = 0;
            List<Integer> temp = new ArrayList<>();
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode<Integer> node = queue.poll();
                temp.add(node.value);
                if (node.left != null) {
                    queue.offer(node.left);
                    cnt++;
                }
                if (node.right != null) {
                    queue.offer(node.right);
                    cnt++;
                }
            }
            results.add(0, temp);
            levelCnt = cnt;
        }

        return results;
    }

    public static void main(String... args) {
        ReverseLevelOrderTraversal unit = new ReverseLevelOrderTraversal();
        System.out.println(unit.reverseTraversal(new BinaryTreeNode<>(12, new BinaryTreeNode<>(7, new BinaryTreeNode<>(9, null, null), null), new BinaryTreeNode<>(1, new BinaryTreeNode<>(10, null, null), new BinaryTreeNode<>(5, null, null)))));
    }
}
