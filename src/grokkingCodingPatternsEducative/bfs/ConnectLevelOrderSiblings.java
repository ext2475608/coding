package grokkingCodingPatternsEducative.bfs;

import common.BinaryTreeNode;
import common.PrinterUtils;

import java.util.*;

public class ConnectLevelOrderSiblings {
    /**
     * Given a binary tree, connect each node with its level order successor. 
     * The last node of each level should point to a null node.
     */
    public void connect(BinaryTreeNode<Integer> root) {
        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while (!queue.isEmpty()) {
            BinaryTreeNode<Integer> prev = null;
            
            int cnt = 0;
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode<Integer> curr = queue.poll();

                if (prev == null) {
                    prev = curr;
                } else {
                    prev.next = curr;
                    prev = prev.next;
                }

                if (curr.left != null) {
                    queue.offer(curr.left);
                    cnt++;
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                    cnt++;
                }
            }
            prev.next = null;
            levelCnt = cnt;
        }
    }

    public static void main(String... args) {
        ConnectLevelOrderSiblings unit = new ConnectLevelOrderSiblings();
        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4, null, null), new BinaryTreeNode<>(5, null, null)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(6, null, null), new BinaryTreeNode<>(7, null, null)));
        unit.connect(root);
        PrinterUtils.print(root);
    }
}
