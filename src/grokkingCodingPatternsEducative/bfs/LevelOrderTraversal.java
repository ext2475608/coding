package grokkingCodingPatternsEducative.bfs;

import common.BinaryTreeNode;

import java.util.*;

public class LevelOrderTraversal {
    
    /**
     Given a binary tree, populate an array to represent its level-by-level traversal. 
     You should populate the values of all nodes of each level from left to right in separate sub-arrays.
     */
    public List<List<Integer>> levelOrder(BinaryTreeNode<Integer> root) {
        List<List<Integer>> results = new ArrayList<>();
        
        if (root == null) {
            return results;
        }

        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while(!queue.isEmpty()) {
            int cnt = 0;
            List<Integer> temp = new ArrayList<>();
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode<Integer> node = queue.poll();
                temp.add(node.value);
                if (node.left != null) {
                    queue.offer(node.left);
                    cnt++;
                }
                if (node.right != null) {
                    queue.offer(node.right);
                    cnt++;
                }
            }
            results.add(temp);
            levelCnt = cnt;
        }

        return results;
    }

    public static void main(String... args) {
        LevelOrderTraversal unit = new LevelOrderTraversal();
        System.out.println(unit.levelOrder(new BinaryTreeNode<>(12, new BinaryTreeNode<>(7, new BinaryTreeNode<>(9, null, null), null), new BinaryTreeNode<>(1, new BinaryTreeNode<>(10, null, null), new BinaryTreeNode<>(5, null, null)))));
    }
}
