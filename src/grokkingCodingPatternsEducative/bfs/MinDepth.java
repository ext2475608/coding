package grokkingCodingPatternsEducative.bfs;

import common.BinaryTreeNode;
import java.util.*;

public class MinDepth {
    /**
     * Find the minimum depth of a binary tree. 
     * The minimum depth is the number of nodes along the shortest path from the root node to the nearest 
     * leaf node.
     */
    public int minDepth(BinaryTreeNode<Integer> root) {
        int minDepth = Integer.MAX_VALUE;

        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while (!queue.isEmpty()) {
            int cnt = 0;
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode<Integer> curr = queue.poll();
                if (curr.left == null && curr.right == null) {
                    minDepth = Math.min(minDepth, levelCnt);
                }
                if (curr.left != null) {
                    queue.offer(curr.left);
                    cnt++;
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                    cnt++;
                }
            }
            levelCnt = cnt;
        }

        return minDepth;
    }

    public static void main(String... args) {
        MinDepth unit = new MinDepth();
        System.out.println(unit.minDepth(new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4, null, null), new BinaryTreeNode<>(5, null, null)), new BinaryTreeNode<>(3, null, null))));
    }
}
