package grokkingCodingPatternsEducative.bfs;

import common.BinaryTreeNode;
import java.util.*;

public class LevelOrderSuccessor {
    
    /**
     * Given a binary tree and a node, find the level order successor of the given node in the tree. 
     * The level order successor is the node that appears right after the given node in the level order traversal.
     */
    public BinaryTreeNode<Integer> levelOrder(BinaryTreeNode<Integer> root, int key) {
        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();

        queue.offer(root);
        boolean returnNext = false;

        while (!queue.isEmpty()) {
            BinaryTreeNode<Integer> curr = queue.poll();
            if (returnNext) {
                return curr;
            }
            if (curr.value == key) {
                returnNext = true;
            }
            if (curr.left != null) {
                queue.offer(curr.left);
            }
            if (curr.right != null) {
                queue.offer(curr.right);
            }
        }

        return null; // this means key was not found
    }

    public static void main(String... args) {
        LevelOrderSuccessor unit = new LevelOrderSuccessor();
        System.out.println(unit.levelOrder(new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4, null, null), new BinaryTreeNode<>(5, null, null)), new BinaryTreeNode<>(3, null, null)), 3).value);
    }

}

