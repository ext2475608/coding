package grokkingCodingPatternsEducative.bfs;

import common.BinaryTreeNode;
import java.util.*;

public class ZigZagTraversal {

    /**
     * Given a binary tree, populate an array to represent its zigzag level order traversal. 
     * You should populate the values of all nodes of the first level from left to right, 
     * then right to left for the next level and keep alternating in the same manner for the following levels
     */
    public List<List<Integer>> zigzag(BinaryTreeNode<Integer> root) {
        List<List<Integer>> results = new ArrayList<>();

        if (root == null) {
            return results;
        }

        int direction = 0; // 0 = left-to-right, 1 = right-to-left
        
        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while (!queue.isEmpty()) {
            int cnt = 0;
            List<Integer> temp = new ArrayList<>();
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode<Integer> curr = queue.poll();
                if (direction == 0) {
                    temp.add(curr.value);
                } else {
                    temp.add(0, curr.value);
                }

                if (curr.left != null) {
                    queue.offer(curr.left);
                    cnt++;
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                    cnt++;
                }
            }
            if (direction == 0) {
                direction = 1;
            } else {
                direction = 0;
            }
            levelCnt = cnt;
            results.add(new ArrayList<>(temp));
        }

        return results;
    }

    public static void main(String... args) {
        ZigZagTraversal unit = new ZigZagTraversal();
        System.out.println(unit.zigzag(new BinaryTreeNode<>(12, new BinaryTreeNode<>(7, new BinaryTreeNode<>(9, null, null), null), new BinaryTreeNode<>(1, new BinaryTreeNode<>(10, null, null), new BinaryTreeNode<>(5, null, null)))));

    }

}
