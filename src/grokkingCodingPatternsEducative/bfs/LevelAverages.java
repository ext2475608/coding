package grokkingCodingPatternsEducative.bfs;

import common.BinaryTreeNode;
import java.util.*;

public class LevelAverages {
    
    /**
     * Given a binary tree, populate an array to represent the averages of all of its levels.
     */
    public List<Double> levelAverages(BinaryTreeNode<Integer> root) {
        List<Double> results = new ArrayList<>();

        if (root == null) {
            return results;
        }

        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while(!queue.isEmpty()) {
            int cnt = 0;
            int sum = 0;
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode<Integer> node = queue.poll();
                sum += node.value;
                if (node.left != null) {
                    queue.offer(node.left);
                    cnt++;
                }
                if (node.right != null) {
                    queue.offer(node.right);
                    cnt++;
                }
            }
            results.add(sum/levelCnt*1.0);
            levelCnt = cnt;
        }

        return results;
    }

    public static void main(String... args) {
        LevelAverages unit = new LevelAverages();
        System.out.println(unit.levelAverages(new BinaryTreeNode<>(12, new BinaryTreeNode<>(7, new BinaryTreeNode<>(9, null, null), null), new BinaryTreeNode<>(1, new BinaryTreeNode<>(10, null, null), new BinaryTreeNode<>(5, null, null)))));
    }

}
