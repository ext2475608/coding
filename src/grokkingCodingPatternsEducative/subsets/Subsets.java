package grokkingCodingPatternsEducative.subsets;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class Subsets {
    
    public List<List<Integer>> iterative(List<Integer> nums) {
        List<List<Integer>> results = new ArrayList<>();

        results.add(new ArrayList<>()); // add the empty subset.

        for (int i=0; i<nums.size(); i++) {
            int currNum = nums.get(i);

            int n = results.size();
            for (int j=0; j<n; j++) {
                List<Integer> currSubset = new ArrayList<>(results.get(j)); // deep copy to a new list.
                currSubset.add(currNum);
                results.add(currSubset);
            }
        }

        return results;
    }

    public static void main(String... args) {
        Subsets unit = new Subsets();

        System.out.println(unit.iterative(List.of(1, 3)));
        assertEquals(List.of(List.of(), List.of(1), List.of(3), List.of(1,3)), unit.iterative(List.of(1, 3)));
        System.out.println(unit.iterative(List.of(1, 5, 3)));
    }

}
