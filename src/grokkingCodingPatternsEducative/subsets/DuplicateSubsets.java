package grokkingCodingPatternsEducative.subsets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DuplicateSubsets {

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> subsets = new ArrayList<>();

        Arrays.sort(nums); // sort the array so duplicates are next to each other.

        subsets.add(new ArrayList<>());

        int start = 0, end = 0;

        for (int i=0; i<nums.length; i++) {
            int currNum = nums[i];

            if (i > 0 && nums[i] == nums[i-1]) { // duplicate spotted.
                start = end+1; // the last end is at the last subset index created in the previous iteration, we move start there so we only use the subsets created in the previous iteration.
            }
            end = subsets.size()-1;
            for (int j=start; j<=end; j++) {
                List<Integer> currSubset = new ArrayList<>(subsets.get(j));
                currSubset.add(currNum);
                subsets.add(currSubset);
            }

        }

        return subsets;
    }

    public static void main(String... args) {
        DuplicateSubsets unit = new DuplicateSubsets();

        System.out.println(unit.subsets(new int[]{1, 3, 3}));
        System.out.println(unit.subsets(new int[]{1, 5, 3, 3}));
    }


}
