package grokkingCodingPatternsEducative.subsets;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PermutationsIterative {

    public List<List<Integer>> premute(int[] nums) {
        List<List<Integer>> results = new ArrayList<>();
        Queue<List<Integer>> permutations = new LinkedList<>();
        permutations.add(new ArrayList<>());

        for (int currNum : nums) { // first loop is to go through all the numbers we need to insert at all positions per permutation.
            int n = permutations.size();
            for (int i=0; i<n; i++) { // second loop is to go through all prospective permutations created so far. Prospective because initially they will be of length lesser than the size of the input array, aka, not a permutation.
                List<Integer> oldPermutation = permutations.poll();

                for (int j=0; j<= oldPermutation.size(); j++) { // third loop is to create a new prospective permutation by insert currNum in place j for all elements.
                    List<Integer> newPermutation = new ArrayList<>(oldPermutation);
                    newPermutation.add(j, currNum);
                    
                    if (newPermutation.size() == nums.length) {
                        results.add(newPermutation); // only add to result if it contains all elements in the array.
                    } else {
                        permutations.add(newPermutation);
                    }
                }
            }
        }
        
        return results;
    }

    public static void main(String... args) {
        PermutationsIterative unit = new PermutationsIterative();

        System.out.println(unit.premute(new int[]{1, 2, 3}));
        System.out.println(unit.premute(new int[]{1,3,5}));
    }
    
}
