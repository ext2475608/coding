package grokkingCodingPatternsEducative.kWayMerge;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.PriorityQueue;

import common.Pair;

public class KthSmallestNumber {
    
    public int kthSmallest(List<int[]> numbers, int k) {
        PriorityQueue<Pair<Integer,Integer>> minHeap = new PriorityQueue<>( (a,b) -> numbers.get(a.first)[a.second]-numbers.get(b.first)[b.second] );

        // insert the first elements
        int idx = 0;
        for (int[] arr : numbers) {
            if (arr.length > 0) {
                minHeap.offer(new Pair<Integer,Integer>(idx++, 0));
            }
        }

        Pair<Integer, Integer> current = null;
        while (!minHeap.isEmpty() && k > 0) {
            current = minHeap.poll();
            if (current.second+1 < numbers.get(current.first).length) {
                minHeap.offer(new Pair<Integer,Integer>(current.first, current.second+1));
            }
            k--;
        }

        return current != null ? numbers.get(current.first)[current.second] : -1;
    }

    public static void main(String... args) {
        KthSmallestNumber unit = new KthSmallestNumber();

        assertEquals(4, unit.kthSmallest(List.of(new int[]{2, 6, 8}, new int[]{3, 6, 7}, new int[]{1, 3, 4}), 5));
        assertEquals(7, unit.kthSmallest(List.of(new int[]{5, 8, 9}, new int[]{1, 7}), 3));
    }

}
