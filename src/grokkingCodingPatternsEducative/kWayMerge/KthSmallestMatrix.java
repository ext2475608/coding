package grokkingCodingPatternsEducative.kWayMerge;

import static org.junit.Assert.assertEquals;

import java.util.PriorityQueue;

import common.Pair;

public class KthSmallestMatrix {

    public int kthSmallest(int[][] matrix, int k) {
        PriorityQueue<Pair<Integer, Integer>> minHeap = new PriorityQueue<>( (a,b) -> matrix[a.first][a.second]-matrix[b.first][b.second] );

        // insert the root elements
        for (int i=0; i<matrix.length; i++) {
            minHeap.offer(new Pair<Integer,Integer>(i, 0));
        }

        Pair<Integer, Integer> current = null;
        while (!minHeap.isEmpty() && k>0) {
            current = minHeap.poll();

            if (current.second+1 < matrix[0].length) {
                minHeap.offer(new Pair<Integer,Integer>(current.first, current.second+1));
            }
            
            k--;
        }

        return matrix[current.first][current.second];
    }

    public static void main(String... args) {
        KthSmallestMatrix unit = new KthSmallestMatrix();
        
        int[][] matrix = new int[3][3];
        matrix[0] = new int[]{2, 6, 8};
        matrix[1] = new int[]{3, 7, 10};
        matrix[2] = new int[]{5, 8, 11};
        assertEquals(7, unit.kthSmallest(matrix, 5));
    }

}
