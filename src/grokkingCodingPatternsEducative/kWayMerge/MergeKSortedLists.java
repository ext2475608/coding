package grokkingCodingPatternsEducative.kWayMerge;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import common.ListNode;

public class MergeKSortedLists {
    
    public List<Integer> merge(List<ListNode<Integer>> numbers) {
        List<Integer> mergedList = new ArrayList<>();
        PriorityQueue<ListNode<Integer>> minHeap = new PriorityQueue<>( (a,b) -> a.val - b.val );

        // populate heap with the smallest element from all lists.
        for (ListNode<Integer> node : numbers) {
            minHeap.offer(node);
        }

        while (!minHeap.isEmpty()) {
            ListNode<Integer> current = minHeap.poll();

            mergedList.add(current.val);

            // add the next node to the heap, if it exists.
            if (current.next != null) {
                minHeap.offer(current.next);
            }
        }

        return mergedList;
    }
    public static void main(String... args) {
        MergeKSortedLists unit = new MergeKSortedLists();

        /**
         * Input: L1=[2, 6, 8], L2=[3, 6, 7], L3=[1, 3, 4]
        Output: [1, 2, 3, 3, 4, 6, 6, 7, 8]
         */
        ListNode<Integer> l1 = new ListNode<Integer>(2, new ListNode<>(6, new ListNode<>(8)));
        ListNode<Integer> l2 = new ListNode<Integer>(3, new ListNode<>(6, new ListNode<>(7)));
        ListNode<Integer> l3 = new ListNode<Integer>(1, new ListNode<>(3, new ListNode<>(4)));

        assertEquals(List.of(1, 2, 3, 3, 4, 6, 6, 7, 8), unit.merge(List.of(l1, l2, l3)));

        /**
         * Input: L1=[5, 8, 9], L2=[1, 7]
        Output: [1, 5, 7, 8, 9]
         */
        l1 = new ListNode<Integer>(5, new ListNode<>(8, new ListNode<>(9)));
        l2 = new ListNode<Integer>(1, new ListNode<>(7));

        assertEquals(List.of(1, 5, 7, 8, 9), unit.merge(List.of(l1, l2)));
    }

}
