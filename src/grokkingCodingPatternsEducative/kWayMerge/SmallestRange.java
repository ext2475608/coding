package grokkingCodingPatternsEducative.kWayMerge;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.PriorityQueue;

import common.Pair;

public class SmallestRange {

    public Pair<Integer, Integer> findSmallestRange(List<int[]> arrays) {
        PriorityQueue<Pair<Integer, Integer>> minHeap = new PriorityQueue<>( (a,b) -> arrays.get(a.first)[a.second] - arrays.get(b.first)[b.second] );
        int max = Integer.MIN_VALUE;
        Pair<Integer, Integer> range = new Pair<Integer,Integer>(Integer.MIN_VALUE/10, Integer.MAX_VALUE/10);

        // insert first elements
        for (int i=0; i<arrays.size(); i++) {
            minHeap.offer(new Pair<Integer,Integer>(i, 0));
            max = Math.max(max, arrays.get(i)[0]);
        }

        while (minHeap.size() == arrays.size()) { // condition to make sure all lists are represented
            Pair<Integer, Integer> current = minHeap.poll();

            range = modifyRange(arrays.get(current.first)[current.second], max, range);

            int newIndex = current.second+1;
            if (newIndex < arrays.get(current.first).length) {
                minHeap.offer(new Pair<Integer,Integer>(current.first, newIndex));
                max = Math.max(max, arrays.get(current.first)[newIndex]);
            }
        }

        return range;
    }

    private Pair<Integer, Integer> modifyRange(int current, int max, Pair<Integer, Integer> oldRange) {
        int newRangeDiff = max-current;
        int oldRangeDiff = oldRange.second-oldRange.first;

        return newRangeDiff < oldRangeDiff ? new Pair<Integer, Integer>(current, max) : oldRange;
    }

    public static void main(String... args) {
        SmallestRange unit = new SmallestRange();

        assertEquals(new Pair<Integer, Integer>(4,7), unit.findSmallestRange(List.of(
            new int[]{1, 5, 8},
            new int[]{4, 12},
            new int[]{7, 8, 10}
        )));

        assertEquals(new Pair<Integer, Integer>(9,12), unit.findSmallestRange(List.of(
            new int[]{1, 9},
            new int[]{4, 12},
            new int[]{7, 10, 16}
        )));
    }

}
