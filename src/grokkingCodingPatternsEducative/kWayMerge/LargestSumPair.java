package grokkingCodingPatternsEducative.kWayMerge;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import common.Pair;

public class LargestSumPair {

    public List<Pair<Integer, Integer>> largestSumPairs(int[] arr1, int[] arr2, int k) {
        PriorityQueue<Pair<Integer, Integer>> maxHeap = new PriorityQueue<>( (a,b) -> (b.first+b.second) - (a.first+a.second) );

        for (int i=0; i<arr1.length; i++) {
            for (int j=0; j<arr2.length; j++) {
                maxHeap.offer(new Pair<Integer,Integer>(arr1[i], arr2[j]));
            }
        }

        List<Pair<Integer, Integer>> res = new ArrayList<>();
        while (k > 0) {
            res.add(maxHeap.poll());
            k--;
        }

        return res;
    }

    public static void main(String... args) {
        LargestSumPair unit = new LargestSumPair();

        System.out.println(unit.largestSumPairs(new int[]{9, 8, 2}, new int[]{6, 3, 1}, 3));
        System.out.println(unit.largestSumPairs(new int[]{5, 2, 1}, new int[]{2, -1}, 3));
    }

}
