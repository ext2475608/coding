package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import common.Pair;

public class RearrangeStringKDistApart {
    
    public String rearrange(String str, int k) {
        Map<Character, Integer> frequency = new HashMap<>();
        PriorityQueue<Pair<Character, Integer>> minHeap = new PriorityQueue<>((a,b) -> a.second-b.second);

        char[] cArr = str.toCharArray();

        for (char c : cArr) {
            frequency.put(c, frequency.getOrDefault(c, 0)+1);
        }

        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            minHeap.offer(new Pair<Character,Integer>(entry.getKey(), 0));
        }

        StringBuilder builder = new StringBuilder();

        int idx = 0;
        while (!minHeap.isEmpty()) {
            Pair<Character, Integer> curr = minHeap.poll();

            if (curr.second > idx) { // if the index at which this character should be inserted is higher than the current index, that means there is no viable character to insert here. hence, a string cannot be formed.
                return "";
            }
            
            builder.append(curr.first);

            frequency.put(curr.first, frequency.get(curr.first)-1);

            if (frequency.get(curr.first) > 0) { // if there are no more chars of that type left, we cannot insert the character back into the heap.
                minHeap.offer(new Pair<Character,Integer>(curr.first, curr.second+k));
            }
            
            idx++;
        }

        return builder.toString();
    }

    public static void main(String... args) {
        RearrangeStringKDistApart unit = new RearrangeStringKDistApart();

        assertEquals("pmpm", unit.rearrange("mmpp", 2));
        assertEquals("aba", unit.rearrange("aab", 2));
        assertEquals("", unit.rearrange("aappa", 3));
    }

}
