package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import common.Pair;

public class SchedulingTasks {

    public int schedule(char[] tasks, int k) {
        Map<Character, Integer> frequency = new HashMap<>();

        for (char task : tasks) {
            frequency.put(task, frequency.getOrDefault(task, 0)+1);
        }

        PriorityQueue<Pair<Character, Integer>> minHeap = new PriorityQueue<>((a,b) -> a.second-b.second);
        
        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            minHeap.offer(new Pair<Character,Integer>(entry.getKey(), 0));
        }

        int cpuIntervals = 0;

        while (!minHeap.isEmpty()) {
            Pair<Character, Integer> task = minHeap.poll();
            if (task.second <= cpuIntervals) {
                frequency.put(task.first, frequency.get(task.first)-1);
                if (frequency.get(task.first) > 0) {
                    minHeap.offer(new Pair<Character,Integer>(task.first, cpuIntervals+k));
                }
            } else {
                minHeap.offer(task); // idle cycle.
            }
            cpuIntervals++;
        }

        return cpuIntervals+1; // +1 to account for 0-based index.
    }

    public static void main(String... args) {
        SchedulingTasks unit = new SchedulingTasks();

        assertEquals(7, unit.schedule(new char[]{'a', 'a', 'a', 'b', 'c', 'c'}, 2));
        assertEquals(5, unit.schedule(new char[]{'a', 'b', 'a'}, 3));
    }

}
