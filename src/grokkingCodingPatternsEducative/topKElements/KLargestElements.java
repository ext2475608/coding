package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class KLargestElements {
    
    public List<Integer> kLargest(int[] nums, int k) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>( (a,b) -> b-a);

        // insert in queue
        for (int num : nums) {
            maxHeap.offer(num);
        }

        List<Integer> results = new ArrayList<>();
        while (k > 0) {
            results.add(maxHeap.poll());
            k--;
        }

        return results;
    }

    public static void main(String... args) {
        KLargestElements unit = new KLargestElements();

        assertEquals(List.of(12, 11, 5), unit.kLargest(new int[]{3, 1, 5, 12, 2, 11}, 3));
        assertEquals(List.of(12, 12, 11), unit.kLargest(new int[]{5, 12, 11, -1, 12}, 3));
    }

}
