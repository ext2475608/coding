package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class KClosestNumbers {
    
    public List<Integer> kClosest(int[] nums, int x, int k) {
        int closestNumberIdx = findClosestBinSearch(nums, x);

        PriorityQueue<Integer> minHeap = new PriorityQueue<>((a,b) -> Math.abs(x-a) - Math.abs(x-b));
        minHeap.offer(nums[closestNumberIdx]);
        for (int i=closestNumberIdx+1; i< Math.min(k+1, nums.length); i++) {
            minHeap.offer(nums[i]);
        }
        for (int i=closestNumberIdx-1; i>= Math.max(0, (closestNumberIdx-k)); i--) {
            minHeap.offer(nums[i]);
        }

        List<Integer> results = new ArrayList<>();
        while (k>0) {
            results.add(minHeap.poll());
            k--;
        }

        Collections.sort(results); //un-necessary, but easier to do asserts.

        return results;
    }

    private int findClosestBinSearch(int[] nums, int k) {
        int low = 0, high = nums.length-1;

        while (low < high) {
            int mid = low + (high-low) / 2;

            if (nums[mid] == k) {
                return mid;
            } else if (k < nums[mid]) {
                high = mid-1;
            } else {
                low = mid+1;
            }
        }

        return low;
    }

    public static void main(String... args) {
        KClosestNumbers unit = new KClosestNumbers();

        assertEquals(List.of(6,7,8), unit.kClosest(new int[]{5, 6, 7, 8, 9}, 7, 3));
        assertEquals(List.of(4, 5, 6), unit.kClosest(new int[]{2, 4, 5, 6, 9}, 6, 3));
        assertEquals(List.of(5, 6, 9), unit.kClosest(new int[]{2, 4, 5, 6, 9}, 10, 3));
    }

}
