package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import common.Point;

public class KClosestPointsToOrigin {

    public List<Point> kClosestPoints(List<Point> points, int k) {
        PriorityQueue<Point> minHeap = new PriorityQueue<>((a,b) -> getDist(a.x, a.y) - getDist(b.x, b.y));

        for (Point point: points) {
            minHeap.offer(point);
        }

        List<Point> results = new ArrayList<>();
        while (k > 0) {
            results.add(minHeap.poll());
            k--;
        }

        return results;
    }

    private int getDist(int x, int y) {
        return (int) (Math.pow(x,2) + Math.pow(y, 2));
    }

    public static void main(String... args) {
        KClosestPointsToOrigin unit = new KClosestPointsToOrigin();
        
        Point first = new Point(1, 2);
        Point second = new Point(1, 3);
        assertTrue(unit.kClosestPoints(List.of(first, second), 1).get(0).equals(first));
    }

}
