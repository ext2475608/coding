package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class TopKFrequentNumbers {
    
    public List<Integer> topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> frequency = new HashMap<>();
        for (int num : nums) {
            frequency.put(num, frequency.getOrDefault(num, 0)+1);
        }

        PriorityQueue<Map.Entry<Integer, Integer>> maxHeap = new PriorityQueue<>((a,b) -> b.getValue()-a.getValue());
        for (Map.Entry<Integer, Integer> entry : frequency.entrySet()) {
            maxHeap.offer(entry);
        }

        List<Integer> results = new ArrayList<>();
        while (k > 0) {
            results.add(maxHeap.poll().getKey());
            k--;
        }

        return results;
    }

    public static void main(String... args) {
        TopKFrequentNumbers unit = new TopKFrequentNumbers();

        assertEquals(List.of(11, 12), unit.topKFrequent(new int[]{1, 3, 5, 12, 11, 12, 11}, 2));
        assertEquals(List.of(11, 12), unit.topKFrequent(new int[]{5, 12, 11, 3, 11}, 2));
    }

}
