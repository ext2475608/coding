package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.PriorityQueue;

public class KthSmallestNumber {
    
    public int kSmallNumber(int[] nums, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int num : nums) {
            minHeap.offer(num);
        }

        int res = -1;
        while (k > 0) {
            res = minHeap.poll();
            k--;
        }

        return res;
    }

    public static void main(String... args) {
        KthSmallestNumber unit = new KthSmallestNumber();

        assertEquals(5, unit.kSmallNumber(new int[]{1, 5, 12, 2, 11, 5}, 3));
        assertEquals(5, unit.kSmallNumber(new int[]{1, 5, 12, 2, 11, 5}, 4));
        assertEquals(11, unit.kSmallNumber(new int[]{5, 12, 11, -1, 12}, 3));
    }

}
