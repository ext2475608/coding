package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.PriorityQueue;

public class SumOfElements {

    public int sum(int[] nums, int k1, int k2) {
        if (k1 > k2) {
            throw new RuntimeException("k1 cannot be greater than k2");
        }

        PriorityQueue<Integer> minHeap = new PriorityQueue<>();

        for (int num : nums) {
            minHeap.offer(num);
        }

        int sum = 0;
        while (!minHeap.isEmpty() && k2 > 1) { // > 1 because we dont want to consider the nth element.
            int curr = minHeap.poll();
            if (k1 <= 0) { // that means these elements are between k1 and k2. If it hits k2, the main loop will terminate.
                sum += curr;
            }
            k1--;
            k2--;
        }

        return sum;
    }

    public static void main(String... args) {
        SumOfElements unit = new SumOfElements();

        assertEquals(23, unit.sum(new int[]{1, 3, 12, 5, 15, 11}, 3, 6));
        assertEquals(12, unit.sum(new int[]{3, 5, 8, 7}, 1, 4));
    }

}
