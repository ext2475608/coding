package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class FrequencyStack {

    int sequenceNumber = 0;
    Map<Integer, Integer> frequency;
    PriorityQueue<FrequencySequence> heap = new PriorityQueue<>();

    public FrequencyStack() {
        this.frequency = new HashMap<>();
        this.heap = new PriorityQueue<FrequencySequence>((a,b) -> {
            if (a.frequency == b.frequency) {
                return b.sequence - a.sequence;
            }
            return b.frequency - a.frequency;
        });
    }

    public void push(int num) {
        frequency.put(num, frequency.getOrDefault(num, 0)+1);
        heap.offer(new FrequencySequence(num, frequency.get(num), sequenceNumber));
        sequenceNumber++;
    }

    public int pop() {
        return heap.poll().key;
    }

    public static void main(String... args) {
        FrequencyStack unit = new FrequencyStack();

        // push(1), push(2), push(3), push(2), push(1), push(2), push(5)
        unit.push(1);
        unit.push(2);
        unit.push(3);
        unit.push(2);
        unit.push(1);
        unit.push(2);
        unit.push(5);

        assertEquals(2, unit.pop());
        assertEquals(1, unit.pop());
        assertEquals(2, unit.pop());
    }

}
