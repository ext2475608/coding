package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class StringSort {
    
    public String sort(String input) {
        Map<Character, Integer> frequency = new HashMap<>();
        for (char c : input.toCharArray()) {
            frequency.put(c, frequency.getOrDefault(c, 0)+1);
        }

        PriorityQueue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>((a,b) -> b.getValue()-a.getValue());
        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            maxHeap.offer(entry);
        }

        StringBuffer buffer = new StringBuffer();
        while (!maxHeap.isEmpty()) {
            Map.Entry<Character, Integer> entry = maxHeap.poll();
            for (int i=0; i<entry.getValue(); i++) {
                buffer.append(entry.getKey());
            }
        }

        return buffer.toString();
    }

    public static void main(String... args) {
        StringSort unit = new StringSort();

        assertEquals("rrggmmPiano", unit.sort("Programming"));
        assertEquals("bbbaac", unit.sort("abcbab"));
        assertEquals("", unit.sort(""));
    }

}
