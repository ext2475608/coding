package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class RearrangeStrings {

    public String rearrange(String str) {
        char[] cArr = str.toCharArray();

        Map<Character, Integer> frequency = new HashMap<>();
        for (char c : cArr) {
            frequency.put(c, frequency.getOrDefault(c, 0)+1);
        }

        PriorityQueue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>((a,b) -> b.getValue()-a.getValue());

        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            maxHeap.offer(entry);
        }

        Map.Entry<Character, Integer> previous = null;
        StringBuilder builder = new StringBuilder();
        while (!maxHeap.isEmpty()) {
            Map.Entry<Character, Integer> curr = maxHeap.poll();
            builder.append(curr.getKey());
            curr.setValue(curr.getValue()-1);

            if (previous != null && previous.getValue() > 0) {
                maxHeap.offer(previous);
            }
            previous = curr;
        }

        return builder.toString();
    }

    public static void main(String... args) {
        RearrangeStrings unit = new RearrangeStrings();

        assertEquals("papap", unit.rearrange("aappp"));
        assertEquals("rgmrgmPiano", unit.rearrange("Programming"));
    }

}
