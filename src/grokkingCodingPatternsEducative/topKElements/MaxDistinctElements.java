package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class MaxDistinctElements {
    
    public int maxDistinct(int[] arr, int k) {
        Map<Integer, Integer> frequency = new HashMap<>();

        for (int i=0; i<arr.length; i++) {
            frequency.put(arr[i], frequency.getOrDefault(arr[i], 0)+1);
        }

        int distinctElements = 0;
        PriorityQueue<Map.Entry<Integer, Integer>> minHeap = new PriorityQueue<>( (a,b) -> a.getValue()-b.getValue());
        for (Map.Entry<Integer, Integer> entry : frequency.entrySet()) {
            if (entry.getValue() > 1) {
                minHeap.offer(entry);
            } else {
                distinctElements++;
            }
        }

        while (k > 0 && !minHeap.isEmpty()) {
            Map.Entry<Integer, Integer> ele = minHeap.poll();
            k -= ele.getValue()-1;
            if (k >= 0) { // if there are enough k's to make this element distinct.
                distinctElements++;
            }
        }

        return k > 0 ? distinctElements - k : distinctElements; // if there are some k's left, we remove the same number of distinct elements.
    }

    public static void main(String... args) {
        MaxDistinctElements unit = new MaxDistinctElements();

        assertEquals(3, unit.maxDistinct(new int[]{7, 3, 5, 8, 5, 3, 3}, 2));
        assertEquals(2, unit.maxDistinct(new int[]{3, 5, 12, 11, 12}, 3));
        assertEquals(3, unit.maxDistinct(new int[]{1, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5}, 2));
    }

}
