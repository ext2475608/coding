package grokkingCodingPatternsEducative.topKElements;

import static org.junit.Assert.assertEquals;

import java.util.PriorityQueue;

public class ConnectRopes {

    public int connect(int[] nums) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int num : nums) {
            minHeap.offer(num);
        }

        int result = 0, temp = 0;
        while (minHeap.size() > 1) {
            // connect two ropes
            temp = minHeap.poll() + minHeap.poll();
            result += temp;

            // add back the connect rope in the heap
            minHeap.offer(temp);
        }

        return result;
    }

    public static void main(String... args) {
        ConnectRopes unit = new ConnectRopes();

        assertEquals(33, unit.connect(new int[]{1, 3, 11, 5}));
        assertEquals(36, unit.connect(new int[]{3, 4, 5, 6}));
        assertEquals(42, unit.connect(new int[]{1, 3, 11, 5, 2}));
    }

}
