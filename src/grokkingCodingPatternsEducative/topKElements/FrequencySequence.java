package grokkingCodingPatternsEducative.topKElements;

public class FrequencySequence {
    public int key;
    public int frequency;
    public int sequence;

    public FrequencySequence(int key, int frequency, int sequence) {
        this.key = key;
        this.frequency = frequency;
        this.sequence = sequence;
    }
}
