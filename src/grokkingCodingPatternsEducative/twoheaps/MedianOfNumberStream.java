package grokkingCodingPatternsEducative.twoheaps;

import static org.junit.Assert.assertEquals;

import java.util.PriorityQueue;

import common.Profiler;

public class MedianOfNumberStream extends Profiler {
    /**
     * Design a class to calculate the median of a number stream. 
     * The class should have the following two methods:

    insertNum(int num): stores the number in the class
    findMedian(): returns the median of all numbers inserted in the class

If the count of numbers inserted in the class is even, the median will be the average of the middle two numbers.

Example 1:

1. insertNum(3)
2. insertNum(1)
3. findMedian() -> output: 2
4. insertNum(5)
5. findMedian() -> output: 3
6. insertNum(4)
7. findMedian() -> output: 3.5
     */

    PriorityQueue<Integer> leftHalf = new PriorityQueue<>((a,b) -> b-a);
    PriorityQueue<Integer> rightHalf = new PriorityQueue<>();

    public void insertNum(int num) {
        startTimer(MedianOfNumberStream.class, "insertNum");
        rightHalf.add(num);

        while (rightHalf.size() > leftHalf.size()) {
            leftHalf.add(rightHalf.poll());
        }
        endTimer();
    }

    public double findMedian() {
        startTimer(MedianOfNumberStream.class, "findMedian");
        double result;
        if ( (leftHalf.size() + rightHalf.size()) % 2 == 0 ) {
            result = (leftHalf.peek() + rightHalf.peek()) / 2.0;
        } else {
            result = leftHalf.peek();
        }
        endTimer();

        return result;
    }

    public static void main(String... args) {
        MedianOfNumberStream unit = new MedianOfNumberStream();

        unit.insertNum(3);
        unit.insertNum(1);
        System.out.println(unit.findMedian());
        assertEquals(2D, unit.findMedian(), 0);
        unit.insertNum(5);
        System.out.println(unit.findMedian());
        assertEquals(3D, unit.findMedian(), 0);
        unit.insertNum(4);
        System.out.println(unit.findMedian());
        assertEquals(3.5, unit.findMedian(), 0);
    }
}
