package grokkingCodingPatternsEducative.dfs;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import common.BinaryTreeNode;
import common.Profiler;

public class CountPathsForSum extends Profiler {
    /**
     * Given a binary tree and a number ‘S’, find all paths in the tree such that the sum of 
     * all the node values of each path equals ‘S’. Please note that the paths can start or end 
     * at any node but all paths must follow direction from parent to child (top to bottom).
     */
    public int countPaths(BinaryTreeNode<Integer> root, int target) {
        startTimer(CountPathsForSum.class);
        int paths = countPaths(root, target, new ArrayList<>());
        endTimer();
        return paths;
    }

    private int countPaths(BinaryTreeNode<Integer> node, int target, List<Integer> temp) {
        if (node == null) {
            return 0;
        }

        temp.add(node.value);
        int pathCnt = 0, pathSum = 0;
        for (int i=temp.size()-1; i>=0; i--) {
            pathSum += temp.get(i);
            if (pathSum == target) {
                pathCnt++;
            }
        }

        pathCnt += countPaths(node.left, target, temp);
        pathCnt += countPaths(node.right, target, temp);

        temp.remove(temp.size()-1);

        return pathCnt;
    }

    public static void main(String... args) {
        CountPathsForSum unit = new CountPathsForSum();

        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1, new BinaryTreeNode<>(7, new BinaryTreeNode<>(6), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(9, new BinaryTreeNode<>(2), new BinaryTreeNode<>(3)));

        int cnt = unit.countPaths(root, 12);
        assertEquals(3, cnt);
    }
}
