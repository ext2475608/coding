package grokkingCodingPatternsEducative.dfs;

import static org.junit.Assert.assertEquals;

import java.util.List;

import common.BinaryTreeNode;
import common.Profiler;

public class PathWithGivenSequence extends Profiler {
    /**
     * Given a binary tree and a number sequence, 
     * find if the sequence is present as a root-to-leaf path in the given tree.
     */
    public boolean isSequencePresent(BinaryTreeNode<Integer> root, List<Integer> sequence) {
        startTimer(PathWithGivenSequence.class);
        boolean isPresent = isPresent(root, sequence, 0);
        endTimer();
        return isPresent;
    }

    private boolean isPresent(BinaryTreeNode<Integer> node, List<Integer> sequence, int idx) {
        if (node == null) {
            // if it has reached so far, it means all nodes must have matched.
            return true;
        }
        if (idx >= sequence.size()) {
            // the path is longer than sequence, cannot match.
            return false;
        }
        if (node.value != sequence.get(idx)) {
            return false;
        }
        idx++;
        boolean left = isPresent(node.left, sequence, idx);
        boolean right = isPresent(node.right, sequence, idx);

        return left || right;
    }

    public static void main(String... args) {
        PathWithGivenSequence unit = new PathWithGivenSequence();

        assertEquals(true, unit.isSequencePresent(new BinaryTreeNode<>(1, new BinaryTreeNode<>(7), new BinaryTreeNode<>(9, new BinaryTreeNode<>(2), new BinaryTreeNode<>(9))), List.of(1,9,9)));
        assertEquals(false, unit.isSequencePresent(new BinaryTreeNode<>(1, new BinaryTreeNode<>(7), new BinaryTreeNode<>(9, new BinaryTreeNode<>(2), new BinaryTreeNode<>(9))), List.of(0,9,0)));
    }
}
