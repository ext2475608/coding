package grokkingCodingPatternsEducative.dfs;

import static org.junit.Assert.assertEquals;

import common.BinaryTreeNode;
import common.Profiler;

public class TreeDiameter extends Profiler {
    /**
     * Given a binary tree, find the length of its diameter. 
     * The diameter of a tree is the number of nodes on the longest path between any two leaf nodes. 
     * The diameter of a tree may or may not pass through the root.

        Note: You can always assume that there are at least two leaf nodes in the given tree.
     */
    int diameter = 0;
    public int treeDiameter(BinaryTreeNode<Integer> root) {
        startTimer(TreeDiameter.class);
        calcDiameter(root);
        endTimer();
        return diameter;
    }

    private int calcDiameter(BinaryTreeNode<Integer> node) {
        if (node == null) {
            return 0;
        }

        int leftHeight = calcDiameter(node.left);
        int rightHeight = calcDiameter(node.right);

        if (leftHeight != 0 && rightHeight != 0) {
            int thisDiameter = leftHeight + rightHeight + 1;
            diameter = Math.max(diameter, thisDiameter);
        }

        return Math.max(leftHeight, rightHeight) + 1; // choose one path, either left subtree path or right + the current node.
    }

    public static void main(String... args) {
        TreeDiameter unit = new TreeDiameter();

        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, null, new BinaryTreeNode<>(4)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(5), new BinaryTreeNode<>(6)));
        System.out.println(unit.treeDiameter(root));
        assertEquals(5, unit.treeDiameter(root));
    }
}
