package grokkingCodingPatternsEducative.dfs;

import static org.junit.Assert.assertTrue;

import common.BinaryTreeNode;
import common.Profiler;

public class PathWithMaxSum extends Profiler {
    /**
     * Find the path with the maximum sum in a given binary tree. Write a function that returns the maximum sum.

A path can be defined as a sequence of nodes between any two nodes and doesn’t necessarily pass through the root. 
The path must contain at least one node.
     */
    int maxSum = 0;
    public int maxSumPath(BinaryTreeNode<Integer> root) {
        startTimer(PathWithMaxSum.class);
        maxSum(root);
        endTimer();
        return maxSum;
    }

    private int maxSum(BinaryTreeNode<Integer> node) {
        if (node == null) {
            return 0;
        }

        int leftSum = maxSum(node.left);
        int rightSum = maxSum(node.right);

        if (leftSum != 0 && rightSum != 0) {
            int sum = leftSum + rightSum + node.value;
            maxSum = Math.max(maxSum, sum);
        }

        return Math.max(leftSum, rightSum) + node.value;
    }

    public static void main(String... args) {
        PathWithMaxSum unit = new PathWithMaxSum();

        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, null, new BinaryTreeNode<>(4)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(5), new BinaryTreeNode<>(6)));
        System.out.println(unit.maxSumPath(root));
        assertTrue(unit.maxSumPath(root) == 16);
    }
}
