package grokkingCodingPatternsEducative.dfs;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import common.BinaryTreeNode;
import common.Profiler;

public class SumOfPathNumbers extends Profiler {
    /**
     * Given a binary tree where each node can only have a digit (0-9) value, 
     * each root-to-leaf path will represent a number. Find the total sum of all the numbers represented by 
     * all paths.
     */
    public int sumOfAllPathNumbers(BinaryTreeNode<Integer> root) {
        startTimer(SumOfPathNumbers.class);
        List<Integer> allNumbers = new ArrayList<>();
        getAllNumbers(root, allNumbers, 0);
        endTimer();
        
        return allNumbers.stream().reduce(0, (a,b)->a+b);
    }

    private void getAllNumbers(BinaryTreeNode<Integer> node, List<Integer> allNumbers, int number) {
        if (node == null) {
            return;
        }

        number = (number*10) + node.value;

        if (node.left == null && node.right == null) {
            allNumbers.add(number);
        } else {
            getAllNumbers(node.left, allNumbers, number);
            getAllNumbers(node.right, allNumbers, number);
        }
    }

    public static void main(String... args) {
        SumOfPathNumbers unit = new SumOfPathNumbers();

        assertEquals(408, unit.sumOfAllPathNumbers(new BinaryTreeNode<>(1, new BinaryTreeNode<>(7), new BinaryTreeNode<>(9, new BinaryTreeNode<>(2), new BinaryTreeNode<>(9)))));
    }
}
