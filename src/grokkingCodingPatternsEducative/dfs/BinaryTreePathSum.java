package grokkingCodingPatternsEducative.dfs;

import static org.junit.Assert.assertTrue;

import common.BinaryTreeNode;

public class BinaryTreePathSum {
    /**
     * Given a binary tree and a number ‘S’, find if the tree has a path from root-to-leaf 
     * such that the sum of all the node values of that path equals ‘S’.
     */
    public boolean pathSum(BinaryTreeNode<Integer> root, int target) {
        if (root == null) {
            if (target == 0) {
                return true;
            } else {
                return false;
            }
        }

        boolean left = pathSum(root.left, target-root.value);
        boolean right = pathSum(root.right, target-root.value);

        return left || right;
    }

    public static void main(String... args) {
        BinaryTreePathSum unit = new BinaryTreePathSum();
        System.out.println(unit.pathSum(new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(6), new BinaryTreeNode<>(7))), 10));
        System.out.println(unit.pathSum(new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(6), new BinaryTreeNode<>(7))), 12));

        assertTrue(unit.pathSum(new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(6), new BinaryTreeNode<>(7))), 10) == true);
        assertTrue(unit.pathSum(new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(3, new BinaryTreeNode<>(6), new BinaryTreeNode<>(7))), 12) == false);
    }

}
