package grokkingCodingPatternsEducative.dfs;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import common.BinaryTreeNode;
import common.Profiler;

public class AllRootToLeafNodes extends Profiler {
    /**
     * Given a binary tree, return all root-to-leaf paths.
     */
    public List<List<Integer>> allPaths(BinaryTreeNode<Integer> root) {
        startTimer(AllRootToLeafNodes.class);
        List<List<Integer>> results = new ArrayList<>();
        allPaths(root, results, new ArrayList<>());
        endTimer();
        return results;
    }

    private void allPaths(BinaryTreeNode<Integer> node, List<List<Integer>> results, List<Integer> temp) {
        if (node == null) {
            return;
        }

        temp.add(node.value);

        if (node.left == null && node.right == null) {
            results.add(new ArrayList<>(temp));
        } else {
            allPaths(node.left, results, temp);
            allPaths(node.right, results, temp);
        }

        temp.remove(temp.size()-1);
    }

    public static void main(String... args) {
        AllRootToLeafNodes unit = new AllRootToLeafNodes();

        assertTrue(unit.allPaths(new BinaryTreeNode<>(1, new BinaryTreeNode<>(7, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(9, new BinaryTreeNode<>(2), new BinaryTreeNode<>(7)))) 
                .equals(List.of(List.of(1,7,4), List.of(1,7,5), List.of(1,9,2), List.of(1,9,7))));
    }

}
