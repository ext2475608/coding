package grokkingCodingPatternsEducative.dfs;

import common.BinaryTreeNode;

import static org.junit.Assert.assertTrue;

import java.util.*;

public class AllPathsForASum {
    /**
     * Given a binary tree and a number ‘S’, 
     * find all paths from root-to-leaf such that the sum of all the node values of each path equals ‘S’.
     */
    public List<List<Integer>> allPaths(BinaryTreeNode<Integer> root, int target) {
        List<List<Integer>> results = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();

        allPaths(root, target, results, temp);

        return results;
    }

    private void allPaths(BinaryTreeNode<Integer> node, int target, List<List<Integer>> results, List<Integer> temp) {
        if (node == null) {
            return;
        }

        temp.add(node.value);

        if (node.value == target && node.left == null && node.right == null) {
            results.add(new ArrayList<>(temp));
        } else {
            allPaths(node.left, target-node.value, results, temp);
            allPaths(node.right, target-node.value, results, temp);
        }

        // backtrack to consider a different path
        temp.remove(temp.size()-1);
    }

    public static void main(String... args) {
        AllPathsForASum unit = new AllPathsForASum();
        System.out.println(unit.allPaths(new BinaryTreeNode<>(1, new BinaryTreeNode<>(7, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(9, new BinaryTreeNode<>(2), new BinaryTreeNode<>(7))), 12));

        assertTrue(unit.allPaths(new BinaryTreeNode<>(1, new BinaryTreeNode<>(7, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(9, new BinaryTreeNode<>(2), new BinaryTreeNode<>(7))), 12) 
                .equals(List.of(List.of(1,7,4), List.of(1,9,2))));
    }

}
