package grokkingCodingPatternsEducative.slidingWindow;

import java.util.*;

public class LongestSubstringWithDistinctChars {
    /**
     * Given a string, find the length of the longest substring, which has all distinct characters.

        Example 1:

        Input: String="aabccbb"
        Output: 3
        Explanation: The longest substring with distinct characters is "abc".

        Example 2:

        Input: String="abbbb"
        Output: 2
        Explanation: The longest substring with distinct characters is "ab".

        Example 3:

        Input: String="abccde"
        Output: 3
        Explanation: Longest substrings with distinct characters are "abc" & "cde".
     */
    public int longestSubstring(String str) {
        Set<Character> seen = new HashSet<>();

        int start = 0, longestSubstringLength = 0;

        for (int end = 0; end < str.length(); end++) {
            char curr = str.charAt(end);
            
            if (!seen.contains(curr)) {
                seen.add(curr);
            } else {
                // repeated char seen
                while (str.charAt(start) != curr) {
                    seen.remove(str.charAt(start));
                    start++;
                }
                // since the loop above will stop at the repeated char, this line removes the repeated char
                seen.remove(str.charAt(start++));
            }

            longestSubstringLength = Math.max(longestSubstringLength, (end-start) + 1);
        }

        return longestSubstringLength;
    }

    public static void main(String... args) {
        LongestSubstringWithDistinctChars unit = new LongestSubstringWithDistinctChars();
        System.out.println(unit.longestSubstring("aabccbb"));
        System.out.println(unit.longestSubstring("abbbb"));
        System.out.println(unit.longestSubstring("abccde"));
    }
}
