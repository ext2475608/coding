package grokkingCodingPatternsEducative.slidingWindow;

import java.util.*;

public class FruitsIntoBaskets {
    /**
     * Given an array of characters where each character represents a fruit tree, you are given two baskets, and your goal is to put maximum number of fruits in each basket. 
     * The only restriction is that each basket can have only one type of fruit.

        You can start with any tree, but you can’t skip a tree once you have started. You will pick one fruit from each tree until you cannot, i.e., you will stop when you have to pick from a third fruit type.

        Write a function to return the maximum number of fruits in both baskets.

        Example 1:

        Input: Fruit=['A', 'B', 'C', 'A', 'C']
        Output: 3
        Explanation: We can put 2 'C' in one basket and one 'A' in the other from the subarray ['C', 'A', 'C']

        Example 2:

        Input: Fruit=['A', 'B', 'C', 'B', 'B', 'C']
        Output: 5
        Explanation: We can put 3 'B' in one basket and two 'C' in the other basket. 
        This can be done if we start with the second letter: ['B', 'C', 'B', 'B', 'C']
     */
    public int fruitsInBasket(char[] fruits) {
        Map<Character, Integer> fruitFrequency = new HashMap<>();

        int start = 0, maxFruits = 0;

        for (int end = 0; end < fruits.length; end++) {
            char curr = fruits[end];

            fruitFrequency.put(curr, fruitFrequency.getOrDefault(curr, 0) + 1);

            while (fruitFrequency.size() > 2) {
                fruitFrequency.put(fruits[start], fruitFrequency.get(fruits[start]) - 1);
                if (fruitFrequency.get(fruits[start]) == 0) {
                    fruitFrequency.remove(fruits[start]);
                }
                start++;
            }

            maxFruits = Math.max(maxFruits, (end - start) + 1);
        }

        return maxFruits;
    }

    public static void main(String... args) {
        FruitsIntoBaskets unit = new FruitsIntoBaskets();
        System.out.println(unit.fruitsInBasket(new char[]{'A', 'B', 'C', 'A', 'C'}));
        System.out.println(unit.fruitsInBasket(new char[]{'A', 'B', 'C', 'B', 'B', 'C'}));
    }
}
