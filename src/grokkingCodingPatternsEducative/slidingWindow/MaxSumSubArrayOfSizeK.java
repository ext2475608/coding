package grokkingCodingPatternsEducative.slidingWindow;

public class MaxSumSubArrayOfSizeK {
    /**
     * Given an array of positive numbers and a positive number ‘k,’ 
     * find the maximum sum of any contiguous subarray of size ‘k’.

        Example 1:

        Input: [2, 1, 5, 1, 3, 2], k=3 
        Output: 9
        Explanation: Subarray with maximum sum is [5, 1, 3].

        Example 2:

        Input: [2, 3, 4, 1, 5], k=2 
        Output: 7
        Explanation: Subarray with maximum sum is [3, 4].
     */
    public int maxSumSubArray(int[] arr, int k) {
        int start = 0, end = 0, maxSum = 0, runningSum = 0;
        
        for (end = 0; end < arr.length; end++) {
            runningSum += arr[end];

            if (end - start + 1 > k) {
                // window size exceeded, move window
                runningSum -= arr[start++];
                // only calculate maxSum if window is of size k
                maxSum = Math.max(maxSum, runningSum);
            }
        }

        return maxSum;
    }

    public static void main(String... args) {
        MaxSumSubArrayOfSizeK unit = new MaxSumSubArrayOfSizeK();
        System.out.println(unit.maxSumSubArray(new int[]{2, 1, 5, 1, 3, 2}, 3));
        System.out.println(unit.maxSumSubArray(new int[]{2, 3, 4, 1, 5}, 2));
        System.out.println(unit.maxSumSubArray(new int[]{1, 2}, 4));
        System.out.println(unit.maxSumSubArray(new int[]{}, 4));
    }
}
