package grokkingCodingPatternsEducative.slidingWindow;

import java.util.*;

public class LongestSubstring {
    /**
     * Given a string, find the length of the longest substring in it with no more than K distinct characters.

        Example 1:

        Input: String="araaci", K=2
        Output: 4
        Explanation: The longest substring with no more than '2' distinct characters is "araa".

        Example 2:

        Input: String="araaci", K=1
        Output: 2
        Explanation: The longest substring with no more than '1' distinct characters is "aa".

        Example 3:

        Input: String="cbbebi", K=3
        Output: 5
        Explanation: The longest substrings with no more than '3' distinct characters are "cbbeb" & "bbebi".

        Example 4:

        Input: String="cbbebi", K=10
        Output: 6
        Explanation: The longest substring with no more than '10' distinct characters is "cbbebi".
     */
    public int longestSubstring(String str, int k) {
        Map<Character, Integer> charFrequecy = new HashMap<>();

        int start = 0, maxSubstringLen = 0, end = 0;

        for (end = 0; end < str.length(); end++) {
            char curr = str.charAt(end);
            charFrequecy.put(curr, charFrequecy.getOrDefault(curr, 0) + 1);

            if (charFrequecy.size() > k) {
                while (charFrequecy.size() > k) {
                    charFrequecy.put(str.charAt(start), charFrequecy.get(str.charAt(start))-1);
                    if (charFrequecy.get(str.charAt(start)) == 0) {
                        charFrequecy.remove(str.charAt(start));
                    }
                    start++;
                }
            }
            maxSubstringLen = Math.max(maxSubstringLen, (end-start)+1);
        }
        return maxSubstringLen == 0 ? (end-start)+1 : maxSubstringLen;
    }

    public static void main(String... args) {
        LongestSubstring unit = new LongestSubstring();
        System.out.println(unit.longestSubstring("araaci", 2));
        System.out.println(unit.longestSubstring("araaci", 1));
        System.out.println(unit.longestSubstring("cbbebi", 3));
        System.out.println(unit.longestSubstring("cbbebi", 10));
    }
}
