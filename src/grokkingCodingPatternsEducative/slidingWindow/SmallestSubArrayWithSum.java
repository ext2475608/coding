package grokkingCodingPatternsEducative.slidingWindow;

public class SmallestSubArrayWithSum {
    /**
     * Given an array of positive numbers and a positive number ‘S,’ 
     * find the length of the smallest contiguous subarray whose sum is greater than or equal to ‘S’. 
     * Return 0 if no such subarray exists.

        Example 1:

        Input: [2, 1, 5, 2, 3, 2], S=7 
        Output: 2
        Explanation: The smallest subarray with a sum greater than or equal to '7' is [5, 2].

        Example 2:

        Input: [2, 1, 5, 2, 8], S=7 
        Output: 1
        Explanation: The smallest subarray with a sum greater than or equal to '7' is [8].

        Example 3:

        Input: [3, 4, 1, 1, 6], S=8 
        Output: 3
        Explanation: Smallest subarrays with a sum greater than or equal to '8' are [3, 4, 1] 
        or [1, 1, 6].
     */
    public int smallestSubArray(int[] arr, int sum) {
        int start = 0, end = 0, smallestSubArrLen = Integer.MAX_VALUE, runningSum = 0;

        for (end = 0; end < arr.length; end++) {
            runningSum += arr[end];

            if (runningSum >= sum) {
                // shrink window 
                while (runningSum >= sum) {
                    // int currentLen = (end-start)+1; // +1 for 0-index
                    // smallestSubArrLen = Math.min(smallestSubArrLen, currentLen); // till the runningSum >=, keep calculating minLen
                    runningSum -= arr[start];
                    start++;
                }
                smallestSubArrLen = Math.min(smallestSubArrLen, (end-start)+2);
            }
        }
        return smallestSubArrLen == Integer.MAX_VALUE ? 0 : smallestSubArrLen;
    }

    public static void main(String... args) {
        SmallestSubArrayWithSum unit = new SmallestSubArrayWithSum();
        System.out.println(unit.smallestSubArray(new int[]{2, 1, 5, 2, 8}, 7));
        System.out.println(unit.smallestSubArray(new int[]{2, 1, 5, 2, 3, 2}, 7));
        System.out.println(unit.smallestSubArray(new int[]{3, 4, 1, 1, 6}, 8));
    }
}
