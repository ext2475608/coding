package grokkingCodingPatternsEducative.slidingWindow;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class LongestSubstringAfterReplacement {

    public int replaceChars(String str, int k) {
        int windowStart = 0, maxLength = 0, maxRepeatingChar = 0;
        Map<Character, Integer> charFreqCount = new HashMap<>();

        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            charFreqCount.put(str.charAt(windowEnd), charFreqCount.getOrDefault(str.charAt(windowEnd), 0) + 1);
            maxRepeatingChar = Math.max(maxRepeatingChar, charFreqCount.get(str.charAt(windowEnd)));

            // shrink window if it makes sense
            while (windowEnd - windowStart + 1 - maxRepeatingChar > k) { // we subtract maxRepeatingChar because we only need to replace the left-over chars
                char leftChar = str.charAt(windowStart);
                charFreqCount.put(leftChar, charFreqCount.get(leftChar)-1);
                windowStart++;
            }

            maxLength = Math.max(maxLength, windowEnd-windowStart+1);
        }

        return maxLength;
    }

    public static void main(String[] args) {
        LongestSubstringAfterReplacement unit = new LongestSubstringAfterReplacement();
        assertEquals(5, unit.replaceChars("aabccbb", 2));
        assertEquals(4, unit.replaceChars("abbcb", 1));
        assertEquals(3, unit.replaceChars("abccde", 1));
    }

}
