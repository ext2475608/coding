package grokkingCodingPatternsEducative.linkedListReversal;

import common.ListNode;
import common.PrinterUtils;

public class ReverseInPlace {

    public ListNode<Integer> reverse(ListNode<Integer> head) {
        if (head == null) {
            return null;
        }

        ListNode<Integer> current = head, previous = null;

        while (current != null) {
            ListNode<Integer> temp = current.next;
            current.next = previous;
            previous = current;
            current = temp;
        }

        return previous;
    }

    public static void main(String... args) {
        ReverseInPlace unit = new ReverseInPlace();

        //2 -> 4 -> 6 -> 8 -> 10 -> null
        ListNode<Integer> head = new ListNode<Integer>(2, new ListNode<>(4, new ListNode<>(6, new ListNode<>(8, new ListNode<>(10)))));
        PrinterUtils.print(unit.reverse(head));

        PrinterUtils.print(unit.reverse(null));

        PrinterUtils.print(unit.reverse(new ListNode<Integer>(1)));
    }

}
