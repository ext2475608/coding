package grokkingCodingPatternsEducative.fastSlowPointer;

import static org.junit.Assert.assertEquals;

import common.ListNode;

public class LinkedListMiddle {

    public ListNode<Integer> findMiddle(ListNode<Integer> head) {
        int totalSize = 0;
        ListNode<Integer> temp = head;
        while (temp != null) {
            totalSize++;
            temp = temp.next;
        }

        int midIdx = totalSize/2;

        while (midIdx > 0) {
            head = head.next;
            midIdx--;
        }

        return head;
    }

    public static void main(String... args) {
        LinkedListMiddle unit = new LinkedListMiddle();

        ListNode<Integer> result = unit.findMiddle(new ListNode<Integer>(1, new ListNode<>(2, new ListNode<>(3, new ListNode<>(4, new ListNode<>(5))))));
        assertEquals(3, (int)(result.val));

        result = unit.findMiddle(new ListNode<Integer>(1, new ListNode<>(2, new ListNode<>(3, new ListNode<>(4, new ListNode<>(5, new ListNode<>(6)))))));
        assertEquals(4, (int)(result.val));
    }

}
