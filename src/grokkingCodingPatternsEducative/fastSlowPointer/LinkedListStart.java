package grokkingCodingPatternsEducative.fastSlowPointer;

import static org.junit.Assert.assertEquals;

import common.ListNode;

public class LinkedListStart {
    
    public ListNode<Integer> findStartPosition(ListNode<Integer> head) {
        ListNode<Integer> temp = head;
        int cycleLength =  findCycleLength(temp);

        ListNode<Integer> ptr1 = head, ptr2 = head;

        for (int i=0; i<cycleLength; i++) {
            ptr2 = ptr2.next;
        }

        while (ptr1 != ptr2) {
            ptr1 = ptr1.next;
            ptr2 = ptr2.next;
        }

        return ptr1;
    }

    private int findCycleLength(ListNode<Integer> head) {
        ListNode<Integer> fast = head, slow = head;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;

            if (slow == fast) {
                ListNode<Integer> third = slow;
                third = third.next;
                int cnt = 1;
                while (slow != third) {
                    third = third.next;
                    cnt++;
                }
                return cnt;
            }
        }

        return -1;
    }

    public static void main(String... args) {
        LinkedListStart unit = new LinkedListStart();

        //1 -> 2 -> 3 -> 4 -> 5 -> 6
        //          |_ _ _ _ _ _ _ |
        ListNode<Integer> six = new ListNode<>(6);
        ListNode<Integer> three = new ListNode<Integer>(3, new ListNode<>(4, new ListNode<>(5, six)));
        six.next = three;
        assertEquals(three, unit.findStartPosition(new ListNode<Integer>(1, new ListNode<>(2, three))));
    }

}
