package grokkingCodingPatternsEducative.fastSlowPointer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import common.ListNode;

public class LinkedListCycle {

    public boolean hasCycle(ListNode<Integer> root) {
        ListNode<Integer> slow = root, fast = root;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow == fast) {
                return true;
            }
        }

        return false;
    }

    public static void main(String... args) {
        LinkedListCycle unit = new LinkedListCycle();

        //1 -> 2 -> 3 -> 4 -> 5 -> 6
        //          |_ _ _ _ _ _ _ |
        ListNode<Integer> six = new ListNode<>(6);
        ListNode<Integer> three = new ListNode<Integer>(3, new ListNode<>(4, new ListNode<>(5, six)));
        six.next = three;
        System.out.println(six.next);
        assertTrue(unit.hasCycle(new ListNode<Integer>(1, new ListNode<>(2, three))));

        // 2 -> 4 -> 6 -> 8 -> 10 -> null
        assertFalse(unit.hasCycle(new ListNode<Integer>(2, new ListNode<>(4, new ListNode<>(6, new ListNode<>(8, new ListNode<>(10)))))));
    }

}
