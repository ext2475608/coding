package grokkingCodingPatternsEducative.fastSlowPointer;

import static org.junit.Assert.assertEquals;

public class HappyNumber {

    public boolean isHappy(int num) {
        int slow = num, fast = num;

        do {
            slow = sq(slow);
            fast = sq(sq(fast));
        } while(slow != fast);

        return slow == 1;
    }

    private int sq(int num) {
        int sq = 0;
        while (num > 0) {
            int digit = num % 10;
            sq += digit*digit;
            num = num/10;
        }
        return sq;
    }

    public static void main(String... args) {
        HappyNumber unit = new HappyNumber();

        assertEquals(true, unit.isHappy(23));
        assertEquals(false, unit.isHappy(12));
    }

}
