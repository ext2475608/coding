package grokkingCodingPatternsEducative.fastSlowPointer;

import static org.junit.Assert.assertEquals;

import common.ListNode;

public class LengthOfCycle {

    public int length(ListNode<Integer> head) {
        ListNode<Integer> slow = head, fast = head;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;

            if (slow == fast) {
                return checkLength(slow);
            }
        }

        return -1; // no cycle
    }

    private int checkLength(ListNode<Integer> slow) {
        ListNode<Integer> third = slow;

        third = third.next;
        int cnt = 1;
        while (third != slow) {
            third = third.next;
            cnt++;
        }

        return cnt;
    }

    public static void main(String... args) {
        LengthOfCycle unit = new LengthOfCycle();

        //1 -> 2 -> 3 -> 4 -> 5 -> 6
        //          |_ _ _ _ _ _ _ |
        ListNode<Integer> six = new ListNode<>(6);
        ListNode<Integer> three = new ListNode<Integer>(3, new ListNode<>(4, new ListNode<>(5, six)));
        six.next = three;
        assertEquals(4, unit.length(new ListNode<Integer>(1, new ListNode<>(2, three))));
    }

}
