package grokkingCodingPatternsEducative.twopointers;

import common.PrinterUtils;

public class DutchNationalFlag {

    /**
     * Given an array containing 0s, 1s and 2s, sort the array in-place. 
     * You should treat numbers of the array as objects, hence, we can’t count 0s, 1s, and 2s to recreate the array.

        The flag of the Netherlands consists of three colors: red, white and blue; 
        and since our input array also consists of three different numbers that is why it is called Dutch National Flag 
        problem.

        Example 1:

        Input: [1, 0, 2, 1, 0]
        Output: [0 0 1 1 2]

        Example 2:

        Input: [2, 2, 0, 1, 2, 0]
        Output: [0 0 1 2 2 2 ]
     */
    public int[] dutchNationalFlag(int[] arr) {
        int low = 0, curr = 0, high = arr.length-1;

        while (curr <= high) {
            if (arr[curr] == 1) {
                curr++;
            } else {
                if (arr[curr] == 0) {
                    swap(arr, curr, low);
                    // incrementing low too for the scenario there is nothing left to curr 
                    low++;
                    curr++;
                } else {
                    swap(arr, curr, high);
                    high--;
                }
            }
        }

        return arr;
    }

    private void swap(int[] arr, int low, int high) {
        int temp = arr[low];
        arr[low] = arr[high];
        arr[high] = temp;
    }

    public static void main(String... args) {
        DutchNationalFlag unit = new DutchNationalFlag();
        PrinterUtils.print(unit.dutchNationalFlag(new int[]{1, 0, 2, 1, 0}));
        PrinterUtils.print(unit.dutchNationalFlag(new int[]{2, 2, 0, 1, 2, 0}));
    }

}
