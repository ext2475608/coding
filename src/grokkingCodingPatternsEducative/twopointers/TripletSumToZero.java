package grokkingCodingPatternsEducative.twopointers;

import java.util.*;

import common.PrinterUtils;
import javafx.util.*;

public class TripletSumToZero {
    /**
     * Given an array of unsorted numbers, find all unique triplets in it that add up to zero.

        Example 1:

        Input: [-3, 0, 1, 2, -1, 1, -2]
        Output: [-3, 1, 2], [-2, 0, 2], [-2, 1, 1], [-1, 0, 1]
        Explanation: There are four unique triplets whose sum is equal to zero.

        Example 2:

        Input: [-5, 2, -1, -2, 3]
        Output: [[-5, 2, 3], [-2, -1, 3]]
        Explanation: There are two unique triplets whose sum is equal to zero.
     */
    public List<int[]> tripletSumToZero(int[] arr) {
        Arrays.sort(arr);

        List<int[]> results = new ArrayList<>();

        for (int i=0; i<arr.length; i++) {
            List<Pair<Integer, Integer>> dupletSum = dupletSum(arr, i);
            if (dupletSum.size() > 0) {
                for (Pair<Integer, Integer> pair : dupletSum) {
                    int[] res = new int[3];
                    res[0] = arr[i];
                    res[1] = pair.getKey();
                    res[2] = pair.getValue();
                    results.add(res);
                }
            }
        }

        return results;
    }

    private List<Pair<Integer, Integer>> dupletSum(int[] arr, int i) {
        int low = i+1, high = arr.length-1, target = 0-arr[i];
        
        List<Pair<Integer, Integer>> results = new ArrayList<>();

        while (low < high) {
            int sum = arr[low] + arr[high];
            if (sum == target) {
                results.add(new Pair<>(arr[low], arr[high]));
                low++;
                high--;
            } else if (sum < target) {
                low++;
            } else {
                high--;
            }
        }

        return results;
    }

    public static void main(String... args) {
        TripletSumToZero unit = new TripletSumToZero();
        PrinterUtils.print(unit.tripletSumToZero(new int[]{-3, 0, 1, 2, -1, 1, -2}));
        PrinterUtils.print(unit.tripletSumToZero(new int[]{-5, 2, -1, -2, 3}));
    }
}
