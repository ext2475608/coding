package grokkingCodingPatternsEducative.twopointers;

public class MinWindowSort {

    /**
     * Given an array, find the length of the smallest subarray in it which when sorted will sort the whole array.

        Example 1:

        Input: [1, 2, 5, 3, 7, 10, 9, 12]
        Output: 5
        Explanation: We need to sort only the subarray [5, 3, 7, 10, 9] to make the whole array sorted

        Example 2:

        Input: [1, 3, 2, 0, -1, 7, 10]
        Output: 5
        Explanation: We need to sort only the subarray [1, 3, 2, 0, -1] to make the whole array sorted

        Example 3:

        Input: [1, 2, 3]
        Output: 0
        Explanation: The array is already sorted

        Example 4:

        Input: [3, 2, 1]
        Output: 3
        Explanation: The whole array needs to be sorted.
     */
    public int minWindowSort(int[] arr) {
        int low = 1, high = arr.length-2;

        while (low < arr.length && arr[low-1] < arr[low] ) {
            low++;
        }
        if (low == arr.length) {
            // arr is already sorted since we couldnt find any element out of place
            return 0;
        }

        while (high > 0 && arr[high] >= arr[high-1]) {
            high--;
        }

        // find the local max and min in the candidate subarray
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i=low; i<=high; i++) {
            min = Math.min(min, arr[i]);
            max = Math.max(max, arr[i]);
        }

        // find all elements outside of candidate array that needs to be moved
        while (low > 0 && arr[low-1] > min) {
            low--;
        }
        while (high < arr.length-1 && arr[high+1] < max) {
            high++;
        }

        return high - low + 1;
    }

    public static void main(String... args) {
        MinWindowSort unit = new MinWindowSort();
        System.out.println(unit.minWindowSort(new int[]{1, 2, 5, 3, 7, 10, 9, 12}));
        System.out.println(unit.minWindowSort(new int[]{1, 3, 2, 0, -1, 7, 10}));
        System.out.println(unit.minWindowSort(new int[]{1, 2, 3}));
        System.out.println(unit.minWindowSort(new int[]{3, 2, 1}));
    }

}
