package grokkingCodingPatternsEducative.twopointers;

import java.util.*;

public class SubArraysWithProductLessThanTarget {
    
    /**
     * Given an array with positive numbers and a positive target number, 
     * find all of its contiguous subarrays whose product is less than the target number.

        Example 1:

        Input: [2, 5, 3, 10], target=30 
        Output: [2], [5], [2, 5], [3], [5, 3], [10]
        Explanation: There are six contiguous subarrays whose product is less than the target.

        Example 2:

        Input: [8, 2, 6, 5], target=50 
        Output: [8], [2], [8, 2], [6], [2, 6], [5], [6, 5] 
        Explanation: There are seven contiguous subarrays whose product is less than the target.
     */
    public List<List<Integer>> productLessThanTarget(int[] arr, int target) {
        List<List<Integer>> results = new ArrayList<>();

        int low = 0;
        double product = 1;

        for (int high = 0; high < arr.length; high++) {
            product *= arr[high];

            while (product >= target && low < arr.length) {
                product /= arr[low++];
            }
                
            List<Integer> temp = new ArrayList<>();
            for (int i=high; i>=low; i--) {
                temp.add(arr[i]);
                results.add(new ArrayList<>(temp));
            }
            
        }

        return results;
    }

    public static void main(String... args) {
        SubArraysWithProductLessThanTarget unit = new SubArraysWithProductLessThanTarget();
        System.out.println(unit.productLessThanTarget(new int[]{2, 5, 3, 10}, 30));
        System.out.println(unit.productLessThanTarget(new int[]{8, 2, 6, 5}, 50));
    }

}
