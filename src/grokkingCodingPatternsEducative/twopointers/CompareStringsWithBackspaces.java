package grokkingCodingPatternsEducative.twopointers;

public class CompareStringsWithBackspaces {
    
    /**
     * Given two strings containing backspaces (identified by the character ‘#’), 
     * check if the two strings are equal.

        Example 1:

        Input: str1="xy#z", str2="xzz#"
        Output: true
        Explanation: After applying backspaces the strings become "xz" and "xz" respectively.

        Example 2:

        Input: str1="xy#z", str2="xyz#"
        Output: false
        Explanation: After applying backspaces the strings become "xz" and "xy" respectively.

        Example 3:

        Input: str1="xp#", str2="xyz##"
        Output: true
        Explanation: After applying backspaces the strings become "x" and "x" respectively.
        In "xyz##", the first '#' removes the character 'z' and the second '#' removes the character 'y'.

        Example 4:

        Input: str1="xywrrmp", str2="xywrrmu#p"
        Output: true
        Explanation: After applying backspaces the strings become "xywrrmp" and "xywrrmp" respectively.
     */

    public boolean compare(String str1, String str2) {
        int ptr1 = str1.length()-1, ptr2 = str2.length()-1;

        while (ptr1 >= 0 && ptr2 >= 0) {
            ptr1 = getNextChar(str1, ptr1);
            ptr2 = getNextChar(str2, ptr2);
            
            if (str1.charAt(ptr1) != str2.charAt(ptr2)) {
                return false;
            }

            ptr1--;
            ptr2--;
        }

        if (ptr1 >= 0 || ptr2 >= 0) {
            return false;
        }

        return true;
    }

    public int getNextChar(String str, int ptr) {
        int backspaceCnt = 0;
        while (ptr >= 0) {
            char curr = str.charAt(ptr);
            if (curr == '#') {
                backspaceCnt++;
            } else if (backspaceCnt > 0) {
                backspaceCnt--;
            } else {
                break;
            }
            ptr--;
        }

        return ptr;
    }

    public static void main(String... args) {
        CompareStringsWithBackspaces unit = new CompareStringsWithBackspaces();
        System.out.println(unit.compare("xp#","xyz##"));
        System.out.println(unit.compare("xy#z","xzz#"));
        System.out.println(unit.compare("xy#z","xyz#"));
        System.out.println(unit.compare("xywrrmp","xywrrmu#p"));
    }

}
