package grokkingCodingPatternsEducative.twopointers;

import common.PrinterUtils;

public class SquareSortedArray {
    /**
     * Given a sorted array, create a new array containing squares of all the numbers of the input array 
     * in the sorted order.

        Example 1:

        Input: [-2, -1, 0, 2, 3]
        Output: [0, 1, 4, 4, 9]

        Example 2:

        Input: [-3, -1, 0, 1, 2]
        Output: [0, 1, 1, 4, 9]
     */
    public int[] sqSortedArray(int[] arr) {
        int low = 0, high = arr.length-1, idx = arr.length-1;
        int[] sqArr = new int[arr.length];

        while (low < high) {
            if ( Math.abs(arr[low]) < Math.abs(arr[high]) ) {
                sqArr[idx] = arr[high] * arr[high];
                high--;
            } else {
                sqArr[idx] = arr[low] * arr[low];
                low++;
            }
            idx--;
        }

        return sqArr;
    }

    public static void main(String... args) {
        SquareSortedArray unit = new SquareSortedArray();
        PrinterUtils.print(unit.sqSortedArray(new int[]{-2, -1, 0, 2, 3}));
        PrinterUtils.print(unit.sqSortedArray(new int[]{-3, -1, 0, 1, 2}));
    }
}
