package grokkingCodingPatternsEducative.twopointers;

import java.util.Arrays;

public class TripletsWithSmallerSum {
    

    /**
     * Given an array arr of unsorted numbers and a target sum, count all triplets in it such that arr[i] + arr[j] + arr[k] < target where i, j, and k 
     * are three different indices. Write a function to return the count of such triplets.

        Example 1:

        Input: [-1, 0, 2, 3], target=3 
        Output: 2
        Explanation: There are two triplets whose sum is less than the target: [-1, 0, 3], [-1, 0, 2]

        Example 2:

        Input: [-1, 4, 2, 1, 3], target=5 
        Output: 4
        Explanation: There are four triplets whose sum is less than the target: 
        [-1, 1, 4], [-1, 1, 3], [-1, 1, 2], [-1, 2, 3]
     */
    public int tripletsCnt(int[] arr, int target) {
        Arrays.sort(arr); // sort it so we can do binary search
        
        int tripletCnt = 0;

        for (int i = 0; i < arr.length-2; i++) {
            if (arr[i] < target) {
                tripletCnt += searchDuplets(arr, i, target);
            }
        }

        return tripletCnt;
    }

    private int searchDuplets(int[] arr, int idx, int target) {
        int low = idx + 1, high = arr.length-1, cnt = 0;

        while (low < high) {
            int sum = arr[idx] + arr[low] + arr[high];

            if (sum < target) {
                // if the sum is less than target, since the array is sorted, all the numbers between low and high would give sum less than target
                for (int i=high; i>low; i--) {
                    cnt++;
                }
                low++;
            } else {
                high--;
            } 
        }

        return cnt;
    }

    public static void main(String... args) {
        TripletsWithSmallerSum unit = new TripletsWithSmallerSum();
        System.out.println(unit.tripletsCnt(new int[]{-1, 0, 2, 3}, 3));
        System.out.println(unit.tripletsCnt(new int[]{-1, 4, 2, 1, 3}, 5));
    }
}
