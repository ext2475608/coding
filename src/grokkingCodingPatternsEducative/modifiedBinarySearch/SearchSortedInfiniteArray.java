package grokkingCodingPatternsEducative.modifiedBinarySearch;

import static org.junit.Assert.assertEquals;

import java.util.Random;

public class SearchSortedInfiniteArray {

    public int search(ArrayReader reader, int key) {
        Random random = new Random();

        int low = 0, high = random.nextInt() & Integer.MAX_VALUE; // zero out the sign bit, so it doesn't give -ve integers

        while (low <= high) {
            int mid = low + (high-low) / 2;

            int num = reader.get(mid);

            if (num == key) {
                return mid;
            }

            if (num == Integer.MAX_VALUE) {
                // array is not that big, search on the left.
                high = mid - 1;
            } else if (key < num) {
                // key is smaller, search on the left.
                high = mid - 1;
            } else {
                // else, search on the right.
                low = mid + 1;
            }
        }

        return -1;
    }

    public static void main(String... args) {
        SearchSortedInfiniteArray unit = new SearchSortedInfiniteArray();

        assertEquals(6, unit.search(new ArrayReader(new int[]{4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30}), 16));
        assertEquals(-1, unit.search(new ArrayReader(new int[]{4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30}), 11));
        assertEquals(4, unit.search(new ArrayReader(new int[]{1, 3, 8, 10, 15}), 15));
        assertEquals(-1, unit.search(new ArrayReader(new int[]{1, 3, 8, 10, 15}), 200));

        ArrayReader reader = new ArrayReader(new int[10000000]);
        reader.set(-2, 0);
        for (int i=1; i<10000000-1; i++) {
            reader.set(i, i);
        }
        assertEquals(0, unit.search(reader, -2));
    }
}
