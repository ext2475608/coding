package grokkingCodingPatternsEducative.modifiedBinarySearch;

import static org.junit.Assert.assertEquals;

public class BitonicArrayMax {
    
    public int findMax(int[] arr) {
        int low = 0, high = arr.length-1;

        while (low < high) {
            int mid = low + (high-low) / 2;
            if (arr[mid] > arr[mid+1]) {
                high = mid;
            } else {
                low = mid + 1;
            }
        }

        return arr[low];
    }

    public static void main(String... args) {
        BitonicArrayMax unit = new BitonicArrayMax();

        assertEquals(12, unit.findMax(new int[]{1, 3, 8, 12, 4, 2}));
        assertEquals(8, unit.findMax(new int[]{3, 8, 3, 1}));
        assertEquals(12, unit.findMax(new int[]{1, 3, 8, 12}));
        assertEquals(10, unit.findMax(new int[]{10, 9, 8}));
    }

}
