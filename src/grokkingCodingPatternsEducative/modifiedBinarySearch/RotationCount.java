package grokkingCodingPatternsEducative.modifiedBinarySearch;

import static org.junit.Assert.assertEquals;

public class RotationCount {
    
    public int cnt(int[] arr) {
        int low = 0, high = arr.length-1;

        if (arr[low] < arr[high]) {
            return 0; // if there was a rotation, the low element has to be greater than the high element, assuming there are no duplicates.
        }

        while (low <= high) {
            int mid = low + (high-low) / 2;

            if (arr[mid] > arr[mid+1]) {
                // this is the pivot point
                return mid + 1; // +1 for 0-based indexes
            }

            if (arr[low] <= arr[mid]) {
                // there is no pivot point in this range
                low = mid + 1;
            } else {
                // there is a pivot point somewhere
                high = mid - 1;
            }
        }

        return -1;
    }

    public static void main(String... args) {
        RotationCount unit = new RotationCount();

        assertEquals(2, unit.cnt(new int[]{10, 15, 1, 3, 8}));
        assertEquals(5, unit.cnt(new int[]{4, 5, 7, 9, 10, -1, 2}));
        assertEquals(0, unit.cnt(new int[]{1, 3, 8, 10}));
    }

}
