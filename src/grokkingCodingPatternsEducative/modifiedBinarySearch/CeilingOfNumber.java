package grokkingCodingPatternsEducative.modifiedBinarySearch;

import common.Profiler;

public class CeilingOfNumber extends Profiler {
    
    public int ceiling(int[] arr, int key) {
        startTimer(CeilingOfNumber.class, "ceiling");

        int low = 0, high = arr.length-1;

        while (low <= high) {
            int mid = low + (high-low) / 2;

            if (arr[mid] == key) {
                endTimer();
                return mid;
            } else if (arr[mid] < key) {
                if (mid + 1 == arr.length) {
                    // no elements to the right
                    endTimer();
                    return -1;
                } else if (arr[mid+1] > key) {
                    endTimer();
                    return mid+1;
                }
                low = mid+1;
            } else {
                high = mid-1;
            }
        }

        endTimer();
        return 0;
    }

    public static void main(String... args) {
        CeilingOfNumber unit = new CeilingOfNumber();
        System.out.println(unit.ceiling(new int[]{4, 6, 10}, 6));
        System.out.println(unit.ceiling(new int[]{1, 3, 8, 10, 15}, 12));
        System.out.println(unit.ceiling(new int[]{4, 6, 10}, 17));
        System.out.println(unit.ceiling(new int[]{4, 6, 10}, -1));
    }

}
