package grokkingCodingPatternsEducative.modifiedBinarySearch;

import static org.junit.Assert.assertEquals;

public class NextLetter {
    
    public char findNextLetter(char[] arr, int key) {
        int low = 0, high = arr.length-1;

        while (low <= high) {
            int mid = low + (high-low) / 2;
            
            if (key < arr[mid]) {
                high = mid-1;
            } else {
                low = mid+1;
            }
        }

        return arr[low % arr.length];
    }

    public static void main(String... args) {
        NextLetter unit = new NextLetter();
        
        assertEquals('h', unit.findNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'f'));
        assertEquals('c', unit.findNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'b'));
        assertEquals('a', unit.findNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'm'));
        assertEquals('a', unit.findNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'h'));
    }

}
