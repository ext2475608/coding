package grokkingCodingPatternsEducative.modifiedBinarySearch;

import common.Profiler;

public class OrderAgnosticBinarySearch extends Profiler {

    public int findKey(int[] arr, int key) {
        startTimer(OrderAgnosticBinarySearch.class, "findKey");

        int ascDesc = 0; // 0 if asc, 1 if desc
        if (arr[0] > arr[arr.length-1]) {
            ascDesc = 1;
        } else if (arr[0] == arr[arr.length-1] && arr[0] == key) {
            // if they are equal, all elements are same
            endTimer();
            return 0;
        } else if (arr[0] == arr[arr.length-1] && arr[0] != key) {
            endTimer();
            return -1;
        }

        int low = 0, high = arr.length-1;

        while (low <= high) {
            int mid = low + (high-low) / 2;
            if (arr[mid] == key) {
                return mid;
            } else if (arr[mid] < key) {
                if (ascDesc == 0) {
                    low = mid+1;
                } else {
                    high = mid-1;
                }
            } else {
                if (ascDesc == 0) {
                    high = mid-1;
                } else {
                    low = mid + 1;
                }
            }
        }

        endTimer();

        return -1;
    }

    public static void main(String... args) {
        OrderAgnosticBinarySearch unit = new OrderAgnosticBinarySearch();
        System.out.println(unit.findKey(new int[]{4, 6, 10}, 10));
        System.out.println(unit.findKey(new int[]{1, 2, 3, 4, 5, 6, 7}, 5));
        System.out.println(unit.findKey(new int[]{10, 6, 4}, 10));
        System.out.println(unit.findKey(new int[]{10, 6, 4}, 4));
    }
    
}
