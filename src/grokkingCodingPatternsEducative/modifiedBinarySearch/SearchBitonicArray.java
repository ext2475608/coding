package grokkingCodingPatternsEducative.modifiedBinarySearch;

import static org.junit.Assert.assertEquals;

public class SearchBitonicArray {
    
    public int search(int[] arr, int key) {
        int low = 0, high = arr.length-1;

        while (low <= high) {
            int mid = low + (high-low) / 2;

            if (arr[mid] == key) {
                return mid;
            }
            
            if (key < arr[mid]) {
                if (isIncreasingRange(arr[mid], arr[mid+1])) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            } else {
                if (isIncreasingRange(arr[mid], arr[mid+1])) {
                    low = mid + 1;
                } else {
                    high = mid - 1;
                }
            }
            
        }

        return -1;
    }

    private boolean isIncreasingRange(int mid, int mid1) {
        return mid <= mid1;
    }

    public static void main(String... args) {
        SearchBitonicArray unit = new SearchBitonicArray();

        assertEquals(3, unit.search(new int[]{1, 3, 8, 4, 3}, 4));
        assertEquals(1, unit.search(new int[]{3, 8, 3, 1}, 8));
        assertEquals(3, unit.search(new int[]{1, 3, 8, 12}, 12));
        assertEquals(0, unit.search(new int[]{10, 9, 8}, 10));
    }

}
