package grokkingCodingPatternsEducative.modifiedBinarySearch;

import static org.junit.Assert.assertEquals;

public class MinDiffElement {
    
    public int searchMinDiffElement(int[] arr, int key) {
        int low = 0, high = arr.length-1;

        while (low <= high) {
            int mid = low + (high-low) / 2;

            if (arr[mid] == key) {
                return arr[mid];
            }

            if (key < arr[mid]) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        
        // take care of index overflows.
        if (high < 0) {
            return arr[low];
        }

        if (low >= arr.length) {
            return arr[high];
        }

        if (Math.abs(arr[low] - key) < Math.abs(arr[high] - key)) {
            return arr[low];
        } else {
            return arr[high];
        }
    }

    public static void main(String... args) {
        MinDiffElement unit = new MinDiffElement();

        assertEquals(6, unit.searchMinDiffElement(new int[]{4, 6, 10}, 7));
        assertEquals(4, unit.searchMinDiffElement(new int[]{4, 6, 10}, 4));
        assertEquals(10, unit.searchMinDiffElement(new int[]{1, 3, 8, 10, 15}, 12));
        assertEquals(10, unit.searchMinDiffElement(new int[]{4, 6, 10}, 17));
    }

}
