package grokkingCodingPatternsEducative.modifiedBinarySearch;

import static org.junit.Assert.assertEquals;

public class SearchRotatedArray {

    public int search(int[] arr, int key) {
        int low = 0, high = arr.length-1;

        while (low <= high) {
            int mid = low + (high-low) / 2;

            if (arr[mid] == key) {
                return mid;
            }

            if (arr[low] <= arr[mid]) { // left side is in ascending order
                if (key < arr[mid]) {
                    high = mid-1;
                } else {
                    low = mid+1;
                }
            } else { // right side is in ascending order
                if (key < arr[mid]) {
                    low = mid+1;
                } else {
                    high = mid-1;
                }
            }
        }

        return -1;
    }

    public static void main(String... args) {
        SearchRotatedArray unit = new SearchRotatedArray();

        assertEquals(1, unit.search(new int[]{10, 15, 1, 3, 8}, 15));
        assertEquals(4, unit.search(new int[]{4, 5, 7, 9, 10, -1, 2}, 10));
    }
    
}
