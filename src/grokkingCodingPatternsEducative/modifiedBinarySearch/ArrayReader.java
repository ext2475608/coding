package grokkingCodingPatternsEducative.modifiedBinarySearch;

public class ArrayReader {
    int[] arr;

  ArrayReader(int[] arr) {
    this.arr = arr;
  }

  public int get(int index) {
    if (index >= arr.length)
      return Integer.MAX_VALUE;
    return arr[index];
  }

  public void set(int num, int index) {
      arr[index] = num;
  }

}
