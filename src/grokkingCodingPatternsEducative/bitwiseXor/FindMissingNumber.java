package grokkingCodingPatternsEducative.bitwiseXor;

import static org.junit.Assert.assertEquals;

public class FindMissingNumber {
    
    public int findMissingNo(int[] arr) {
        int x1 = 1;
        for (int i=2; i<=arr.length+1; i++) {
            System.out.print(Integer.toBinaryString(x1) + " XOR " + Integer.toBinaryString(i));
            x1 = x1 ^ i;
            System.out.println(" = " + Integer.toBinaryString(x1));
        }

        System.out.println("=====");

        int x2 = arr[0];
        for (int i=1; i<arr.length; i++) {
            System.out.print(Integer.toBinaryString(x2) + " XOR " + Integer.toBinaryString(i));
            x2 = x2 ^ arr[i];
            System.out.println(" = " + Integer.toBinaryString(x2));
        }

        return x1 ^ x2;
    }

    public static void main(String... args) {
        FindMissingNumber unit = new FindMissingNumber();

        assertEquals(3, unit.findMissingNo(new int[]{1, 5, 2, 6, 4}));
    }

}
