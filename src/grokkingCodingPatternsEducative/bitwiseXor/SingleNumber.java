package grokkingCodingPatternsEducative.bitwiseXor;

import static org.junit.Assert.assertEquals;

public class SingleNumber {

    public int nonDuplicateNumber(int[] arr) {
        int res = arr[0];
        
        for (int i=1; i< arr.length; i++) {
            res = res ^ arr[i];
        }

        return res;
    }

    public static void main(String... args) {
        SingleNumber unit = new SingleNumber();

        assertEquals(4, unit.nonDuplicateNumber(new int[]{1, 4, 2, 1, 3, 2, 3}));
        assertEquals(9, unit.nonDuplicateNumber(new int[]{7, 9, 7}));
    }
    
}
