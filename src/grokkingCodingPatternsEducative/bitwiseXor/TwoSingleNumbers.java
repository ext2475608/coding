package grokkingCodingPatternsEducative.bitwiseXor;

import static org.junit.Assert.assertArrayEquals;

public class TwoSingleNumbers {
    
    public int[] twoNonRepeatingNumbers(int[] arr) {
        int n3 = 0;
        for (int num : arr) {
            n3 ^= num;
        }

        // get rightMostSetBit
        int rightMostSetBit = 1;
        while ((rightMostSetBit & n3) == 0) { // AND would give 0 till both bits are 1
            rightMostSetBit = rightMostSetBit << 1; // left shift by 1 bit
        }

        int num1 = 0, num2 = 0;
        for (int num : arr) {
            if ( (num & rightMostSetBit) != 0) { // the bit is set group
                num1 ^= num;
            } else { // the bit is not set group
                num2 ^= num;
            }
        }

        return new int[]{num1, num2};
    }

    public static void main(String... args) {
        TwoSingleNumbers unit = new TwoSingleNumbers();

        assertArrayEquals(new int[]{6,4}, unit.twoNonRepeatingNumbers(new int[]{1, 4, 2, 1, 3, 5, 6, 2, 3, 5}));
        assertArrayEquals(new int[]{3,1}, unit.twoNonRepeatingNumbers(new int[]{2, 1, 3, 2}));
    }

}
