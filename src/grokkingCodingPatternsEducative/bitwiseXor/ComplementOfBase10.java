package grokkingCodingPatternsEducative.bitwiseXor;

import static org.junit.Assert.assertEquals;

public class ComplementOfBase10 {

    public int bitwiseComplement(int num) {
        int n = num;
        int cnt = 0;

        while (n > 0) {
            cnt++;
            n = n >> 1; // shift 1 bit
        }

        int allBitsSet = (int) Math.pow(2, cnt) - 1;

        return num ^ allBitsSet;
    }

    public static void main(String... args) {
        ComplementOfBase10 unit = new ComplementOfBase10();

        assertEquals(7, unit.bitwiseComplement(8));
        assertEquals(5, unit.bitwiseComplement(10));
    }

}
