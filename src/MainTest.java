import grokkingCodingPatternsEducative.twopointers.*;
import leetcode.backtracking.*;
import leetcode.maps.easy.TwoSum;
import leetcode.stacks.easy.ValidParens;
import misc.*;
import grokkingCodingPatternsEducative.slidingWindow.*;
import grokkingCodingPatternsEducative.subsets.*;
import grokkingCodingPatternsEducative.topKElements.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import basics.tree_graph_traversals.*;
import basics.trie.Trie;
import blindCuratedList.array.ContainsDuplicate;
import blindCuratedList.array.ProdOfArray;
import blindCuratedList.array.Stocks;
import blindCuratedList.matrix.SetMatrixZeroes;
import common.*;

import grokkingCodingPatternsEducative.bfs.*;
import grokkingCodingPatternsEducative.bitwiseXor.*;
import grokkingCodingPatternsEducative.dfs.*;
import grokkingCodingPatternsEducative.fastSlowPointer.*;
import grokkingCodingPatternsEducative.kWayMerge.KthSmallestMatrix;
import grokkingCodingPatternsEducative.kWayMerge.LargestSumPair;
import grokkingCodingPatternsEducative.kWayMerge.MergeKSortedLists;
import grokkingCodingPatternsEducative.kWayMerge.SmallestRange;
import grokkingCodingPatternsEducative.linkedListReversal.*;
import grokkingCodingPatternsEducative.twoheaps.*;
import grokkingCodingPatternsEducative.modifiedBinarySearch.*;

public class MainTest {

    public static void main(String... args) throws Exception {
        ProfilerLogger.createWriter();
        try {
            List<Callable<Void>> tasks = new ArrayList<>();
            Profiler profiler = new Profiler();

            /**
             * Start Coding Patterns - Educative
             */
            // two pointers
            tasks.add(() -> {profiler.recordTime(PairWithTargetSum.class); return null;});
            tasks.add(() -> {profiler.recordTime(RemoveDuplicates.class); return null;});
            tasks.add(() -> {profiler.recordTime(SquareSortedArray.class); return null;});
            tasks.add(() -> {profiler.recordTime(TripletSumCloseToTarget.class); return null;});
            tasks.add(() -> {profiler.recordTime(TripletsWithSmallerSum.class); return null;});
            tasks.add(() -> {profiler.recordTime(SubArraysWithProductLessThanTarget.class); return null;});
            tasks.add(() -> {profiler.recordTime(DutchNationalFlag.class); return null;});
            tasks.add(() -> {profiler.recordTime(CompareStringsWithBackspaces.class); return null;});
            tasks.add(() -> {profiler.recordTime(MinWindowSort.class); return null;});
            tasks.add(() -> {profiler.recordTime(TripletSumToZero.class); return null;});

            // sliding window
            tasks.add(() -> {profiler.recordTime(FruitsIntoBaskets.class); return null;});
            tasks.add(() -> {profiler.recordTime(LongestSubstring.class); return null;});
            tasks.add(() -> {profiler.recordTime(LongestSubstringWithDistinctChars.class); return null;});
            tasks.add(() -> {profiler.recordTime(MaxSumSubArrayOfSizeK.class); return null;});
            tasks.add(() -> {profiler.recordTime(SmallestSubArrayWithSum.class); return null;});
            // TODO: more left

            // bfs
            tasks.add(() -> {profiler.recordTime(LevelOrderTraversal.class); return null;});
            tasks.add(() -> {profiler.recordTime(ReverseLevelOrderTraversal.class); return null;});
            tasks.add(() -> {profiler.recordTime(ZigZagTraversal.class); return null;});
            tasks.add(() -> {profiler.recordTime(LevelAverages.class); return null;});
            tasks.add(() -> {profiler.recordTime(MinDepth.class); return null;});
            tasks.add(() -> {profiler.recordTime(LevelOrderSuccessor.class); return null;});
            tasks.add(() -> {profiler.recordTime(ConnectLevelOrderSiblings.class); return null;});
            tasks.add(() -> {profiler.recordTime(ConnectAllLevelOrderSiblings.class); return null;});
            tasks.add(() -> {profiler.recordTime(RightView.class); return null;});

            // dfs
            tasks.add(() -> {profiler.recordTime(BinaryTreePathSum.class); return null;});
            tasks.add(() -> {profiler.recordTime(AllPathsForASum.class); return null;});
            tasks.add(() -> {profiler.recordTime(AllRootToLeafNodes.class); return null;});
            tasks.add(() -> {profiler.recordTime(SumOfPathNumbers.class); return null;});
            tasks.add(() -> {profiler.recordTime(PathWithGivenSequence.class); return null;});
            tasks.add(() -> {profiler.recordTime(CountPathsForSum.class); return null;});
            tasks.add(() -> {profiler.recordTime(TreeDiameter.class); return null;});
            tasks.add(() -> {profiler.recordTime(PathWithMaxSum.class); return null;});

            // two heaps
            tasks.add(() -> {profiler.recordTime(MedianOfNumberStream.class); return null;});
            // TODO: more left

            // modified binary search
            tasks.add(() -> {profiler.recordTime(OrderAgnosticBinarySearch.class); return null;});
            tasks.add(() -> {profiler.recordTime(CeilingOfNumber.class); return null;});
            tasks.add(() -> {profiler.recordTime(NextLetter.class); return null;});
            tasks.add(() -> {profiler.recordTime(SearchSortedInfiniteArray.class); return null;});
            tasks.add(() -> {profiler.recordTime(BitonicArrayMax.class); return null;});
            tasks.add(() -> {profiler.recordTime(SearchBitonicArray.class); return null;});
            tasks.add(() -> {profiler.recordTime(SearchRotatedArray.class); return null;});
            tasks.add(() -> {profiler.recordTime(RotationCount.class); return null;});
            tasks.add(() -> {profiler.recordTime(MinDiffElement.class); return null;});

            // bitwise XOR
            tasks.add(() -> {profiler.recordTime(FindMissingNumber.class); return null;});
            tasks.add(() -> {profiler.recordTime(SingleNumber.class); return null;});
            tasks.add(() -> {profiler.recordTime(TwoSingleNumbers.class); return null;});
            tasks.add(() -> {profiler.recordTime(ComplementOfBase10.class); return null;});

            // top 'K' elements
            tasks.add(() -> {profiler.recordTime(KLargestElements.class); return null;});
            tasks.add(() -> {profiler.recordTime(KthSmallestNumber.class); return null;});
            tasks.add(() -> {profiler.recordTime(KClosestPointsToOrigin.class); return null;});
            tasks.add(() -> {profiler.recordTime(ConnectRopes.class); return null;});
            tasks.add(() -> {profiler.recordTime(TopKFrequentNumbers.class); return null;});
            tasks.add(() -> {profiler.recordTime(StringSort.class); return null;});
            tasks.add(() -> {profiler.recordTime(KClosestNumbers.class); return null;});
            tasks.add(() -> {profiler.recordTime(MaxDistinctElements.class); return null;});
            tasks.add(() -> {profiler.recordTime(SumOfElements.class); return null;});
            tasks.add(() -> {profiler.recordTime(RearrangeStrings.class); return null;});
            tasks.add(() -> {profiler.recordTime(RearrangeStringKDistApart.class); return null;});
            tasks.add(() -> {profiler.recordTime(SchedulingTasks.class); return null;});
            tasks.add(() -> {profiler.recordTime(FrequencyStack.class); return null;});

            // 'k' way merge
            tasks.add(() -> {profiler.recordTime(MergeKSortedLists.class); return null;});
            tasks.add(() -> {profiler.recordTime(KthSmallestNumber.class); return null;});
            tasks.add(() -> {profiler.recordTime(KthSmallestMatrix.class); return null;});
            tasks.add(() -> {profiler.recordTime(SmallestRange.class); return null;});
            tasks.add(() -> {profiler.recordTime(LargestSumPair.class); return null;});

            // Subsets
            tasks.add(() -> {profiler.recordTime(Subsets.class); return null;});
            tasks.add(() -> {profiler.recordTime(DuplicateSubsets.class); return null;});
            tasks.add(() -> {profiler.recordTime(PermutationsIterative.class); return null;});
            // TODO: more left

            // Fast and Slow pointers
            tasks.add(() -> {profiler.recordTime(LinkedListCycle.class); return null;});
            tasks.add(() -> {profiler.recordTime(LengthOfCycle.class); return null;});
            tasks.add(() -> {profiler.recordTime(LinkedListStart.class); return null;});
            tasks.add(() -> {profiler.recordTime(HappyNumber.class); return null;});
            tasks.add(() -> {profiler.recordTime(LinkedListMiddle.class); return null;});
            // TODO: more left

            // Backtracking
            tasks.add(() -> {profiler.recordTime(GenerateParens.class); return null;});
            tasks.add(() -> {profiler.recordTime(WordSearch.class); return null;});
            tasks.add(() -> {profiler.recordTime(PhoneNumberLetterCombinations.class); return null;});

            // Linkedlist reversal
            tasks.add(() -> {profiler.recordTime(ReverseInPlace.class); return null;});

            /**
             * End Coding Patterns - Educative
             */


            /**
             * Start misc
             */
            tasks.add(() -> {profiler.recordTime(TwoSum.class); return null;});
            tasks.add(() -> {profiler.recordTime(TwoSumPairs.class); return null;});
            tasks.add(() -> {profiler.recordTime(ClosestSumToTarget.class); return null;});
            tasks.add(() -> {profiler.recordTime(UniquePairs.class); return null;});
            tasks.add(() -> {profiler.recordTime(UniqueTriplets.class); return null;});
            tasks.add(() -> {profiler.recordTime(UniqueQuadruplets.class); return null;});
            tasks.add(() -> {profiler.recordTime(PairTuples.class); return null;});
            tasks.add(() -> {profiler.recordTime(FileHierarchy.class); return null;});

            /**
             * End misc
             */

             /**
             * Start Leetcode
             */
            tasks.add(() -> {profiler.recordTime(TwoSum.class); return null;});
            tasks.add(() -> {profiler.recordTime(ValidParens.class); return null;});
            tasks.add(() -> {profiler.recordTime(Rectangle.class); return null;});
            /**
             * End Leetcode
            */

            /**
             * Start basics
             */
            tasks.add(() -> {profiler.recordTime(DfsTraversals.class); return null;});
            tasks.add(() -> {profiler.recordTime(Trie.class); return null;});
            /**
             * End basics
             */

            /**
             * Start Blind curated list
             */
            tasks.add(() -> {profiler.recordTime(blindCuratedList.array.TwoSum.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.array.MaximumSubarray.class); return null;});
            tasks.add(() -> {profiler.recordTime(Stocks.class); return null;});
            tasks.add(() -> {profiler.recordTime(ContainsDuplicate.class); return null;});
            tasks.add(() -> {profiler.recordTime(ProdOfArray.class); return null;});
            tasks.add(() -> {profiler.recordTime(SetMatrixZeroes.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.matrix.WordSearch.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.matrix.SpiralMatrix.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.tree.SerializeDeserialize.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.tree.Subtree.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.tree.MaxDepthBinaryTree.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.tree.SameTree.class); return null;});
            tasks.add(() -> {profiler.recordTime(blindCuratedList.strings.ValidAnagram.class); return null;});
             /**
             * End Blind curated list
             */

            // Execute solutions in parallel
            ExecutorService executorService = new ThreadPoolExecutor(1, 100, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
            executorService.invokeAll(tasks);
            executorService.shutdown();

        } finally {
            ProfilerLogger.PROFILER_WRITER.close();
        }
    }
    
}
