package blindCuratedList.array;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

public class ContainsDuplicate {
    
    public boolean hasDuplicates(int[] nums) {
        Map<Integer, Integer> numberToCount = new HashMap<>();
        for (int num : nums) {
            numberToCount.put(num, numberToCount.getOrDefault(num, 0) + 1);
        }

        for (int num : nums) {
            if (numberToCount.get(num) > 1) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        ContainsDuplicate unit = new ContainsDuplicate();

        assertEquals(true, unit.hasDuplicates(new int[]{1,2,3,1}));
        assertEquals(false, unit.hasDuplicates(new int[]{1,2,3,4}));
        assertEquals(true, unit.hasDuplicates(new int[]{1,1,1,3,3,4,3,2,4,2}));
        assertEquals(false, unit.hasDuplicates(new int[]{1}));
    }

}
