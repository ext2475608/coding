package blindCuratedList.array;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

public class ProdOfArray {

    public int[] product(int[] nums) {
        int[] output = new int[nums.length];
        Arrays.fill(output, 1);

        if (nums.length == 1) {
            return new int[]{0};
        }

        int leftProduct = nums[0];
        for (int i=1; i<nums.length; i++) {
            output[i] = leftProduct;
            leftProduct *= nums[i];
        }

        int[] output2 = new int[nums.length];
        Arrays.fill(output2, 1);

        int rightProduct = nums[nums.length-1];
        for (int i=nums.length-2; i>=0; i--) {
            output2[i] = rightProduct;
            rightProduct *= nums[i];
        }

        int[] output3 = new int[nums.length];
        for (int i=0; i<nums.length; i++) {
            output3[i] = output[i] * output2[i];
        }

        return output3;
    }

    public static void main(String[] args) {
        ProdOfArray unit = new ProdOfArray();

        assertTrue(Arrays.equals(new int[]{24,12,8,6}, unit.product(new int[]{1,2,3,4})));
        assertTrue(Arrays.equals(new int[]{0,0,9,0,0}, unit.product(new int[]{-1,1,0,-3,3})));
        assertTrue(Arrays.equals(new int[]{2,1}, unit.product(new int[]{1,2})));
    }

}
