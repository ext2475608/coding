package blindCuratedList.array;

import static org.junit.Assert.assertEquals;

public class MaximumSubarray {

    public int maxSubArray(int[] nums) {
        if (nums.length == 0) {
            return -1;
        }

        int runningSum = nums[0];
        int maxSum = Integer.MIN_VALUE;

        for (int num : nums) {
            if (runningSum > num) {
                runningSum += num;
            } else {
                runningSum = num;
            }

            maxSum = Math.max(maxSum, runningSum);
        }


        return maxSum;
    }

    public static void main(String[] args) {
        MaximumSubarray unit = new MaximumSubarray();

        assertEquals(6, unit.maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
        assertEquals(1, unit.maxSubArray(new int[]{1}));
        assertEquals(23, unit.maxSubArray(new int[]{5,4,-1,7,8}));
    }
    
}
