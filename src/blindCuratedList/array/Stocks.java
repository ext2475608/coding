package blindCuratedList.array;

import static org.junit.Assert.assertEquals;

public class Stocks {

    public int maxProfit(int[] stocks) {
        if (stocks.length == 0) {
            return 0;
        }

        int min = stocks[0], maxProfit = 0;

        for (int i=1; i < stocks.length; i++) {
            int currStock = stocks[i];

            min = Math.min(min, currStock);
            maxProfit = Math.max(maxProfit, currStock-min);
        }

        return maxProfit > 0 ? maxProfit : 0;
    }

    public static void main(String[] args) {
        Stocks unit = new Stocks();
        assertEquals(5, unit.maxProfit(new int[]{7,1,5,3,6,4}));
        assertEquals(0, unit.maxProfit(new int[]{7,6,4,3,1}));
        assertEquals(0, unit.maxProfit(new int[]{7}));
        assertEquals(3, unit.maxProfit(new int[]{7,6,4,3,1,2,3,4}));
    }
    
}
