package blindCuratedList.array;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> complements = populateMap(nums);

        for (int i=0; i<nums.length; i++) {
            int complement = target - nums[i];
            if (complements.containsKey(complement) && i != complements.get(complement)) {
                return new int[]{i, complements.get(complement)};
            }
        }

        return new int[]{-1,-1}; // no solution exists.
    }

    private Map<Integer, Integer> populateMap(int[] nums) {
        Map<Integer, Integer> complements = new HashMap<>();

        for (int i=0; i<nums.length; i++) {
            complements.put(nums[i], i);
        }

        return complements;
    }

    public static void main(String[] args) {
        TwoSum unit = new TwoSum();

        assertTrue(Arrays.equals(new int[]{0,1}, unit.twoSum(new int[]{2,7,11,15}, 9)));
        assertTrue(Arrays.equals(new int[]{1,2}, unit.twoSum(new int[]{3,2,4}, 6)));
        assertTrue(Arrays.equals(new int[]{0,1}, unit.twoSum(new int[]{3,3}, 6)));
    }

}
