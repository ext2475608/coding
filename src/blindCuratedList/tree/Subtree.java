package blindCuratedList.tree;

import static org.junit.Assert.assertTrue;

import common.BinaryTreeNode;

public class Subtree {
    public boolean isSubtree(BinaryTreeNode<Integer> root, BinaryTreeNode<Integer> subRoot) {
        return preOrderCheck(root, subRoot);
    }

    private boolean preOrderCheck(BinaryTreeNode<Integer> root, BinaryTreeNode<Integer> subRoot) {
        if (root == null && subRoot != null) {
            return false;
        }

        if (root == null && subRoot == null) {
            return true;
        }

        if (root.equals(subRoot)) { 
            // if there's a match, check both trees for equality.
            return preOrderCheck(root.left, subRoot.left) && preOrderCheck(root.right, subRoot.right);
        }

        // if there's no match, keep traversing root to find a potential start node.
        return preOrderCheck(root.left, subRoot) || preOrderCheck(root.right, subRoot);
    }

    public static void main(String[] args) {
        Subtree unit = new Subtree();

        BinaryTreeNode<Integer> two = new BinaryTreeNode<>(2);
        BinaryTreeNode<Integer> four = new BinaryTreeNode<>(4, new BinaryTreeNode<>(1), two);
        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(3, four, new BinaryTreeNode<>(5));
        assertTrue(unit.isSubtree(root, four));
    }
}
