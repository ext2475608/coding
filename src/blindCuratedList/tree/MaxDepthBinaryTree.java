package blindCuratedList.tree;

import static org.junit.Assert.assertEquals;

import common.BinaryTreeNode;

public class MaxDepthBinaryTree {
    
    public int maxDepth(BinaryTreeNode<Integer> root) {
        if (root == null) {
            return 0;
        }

        int height = Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;

        return height;
    }

    public static void main(String[] args) {
        MaxDepthBinaryTree unit = new MaxDepthBinaryTree();

        assertEquals(3, unit.maxDepth(new BinaryTreeNode<>(3, new BinaryTreeNode<>(9), new BinaryTreeNode<>(20, new BinaryTreeNode<>(15), new BinaryTreeNode<>(7)))));
        assertEquals(2, unit.maxDepth(new BinaryTreeNode<>(1, null, new BinaryTreeNode<>(2))));
        assertEquals(0, unit.maxDepth(null));
    }

}
