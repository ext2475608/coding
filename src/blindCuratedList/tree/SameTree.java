package blindCuratedList.tree;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import common.BinaryTreeNode;

public class SameTree {

    public boolean isSame(BinaryTreeNode<Integer> node1, BinaryTreeNode<Integer> node2) {
        if (node1 == null && node2 == null) {
            return true;
        }
        
        if (node1 == null && node2 != null) {
            return false;
        }

        if (node2 == null && node1 != null) {
            return false;
        }

        if (node1.value != node2.value) {
            return false;
        }

        return isSame(node1.left, node2.left) && isSame(node1.right, node2.right);
    }

    public static void main(String[] args) {
        SameTree unit = new SameTree();

        BinaryTreeNode<Integer> root1 = new BinaryTreeNode<>(1, new BinaryTreeNode<>(2), new BinaryTreeNode<>(3));
        assertTrue(unit.isSame(root1, root1));
        assertFalse(unit.isSame(new BinaryTreeNode<>(1, null, new BinaryTreeNode<>(2)), new BinaryTreeNode<>(1, new BinaryTreeNode<>(2), null)));
        assertFalse(unit.isSame(new BinaryTreeNode<>(1, new BinaryTreeNode<>(1), new BinaryTreeNode<>(2)), new BinaryTreeNode<>(1, new BinaryTreeNode<>(2), new BinaryTreeNode<>(1))));
    }
    
}
