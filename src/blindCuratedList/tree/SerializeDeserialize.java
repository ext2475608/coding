package blindCuratedList.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.LinkedList;

import common.BinaryTreeNode;

public class SerializeDeserialize {
    // Encodes a tree to a single string.
    public String serialize(BinaryTreeNode<Integer> root) {
        StringBuilder serializedString = new StringBuilder();

        preOrder(root, serializedString);
        serializedString.deleteCharAt(serializedString.length()-1);

        return serializedString.toString();
    }

    private void preOrder(BinaryTreeNode<Integer> node, StringBuilder builder) {
        if (node == null) {
            builder.append("null");
            builder.append(",");
            return;
        }
        builder.append(node.value);
        builder.append(",");
        preOrder(node.left, builder);
        preOrder(node.right, builder);
    }

    // Decodes your encoded data to tree.
    public BinaryTreeNode<Integer> deserialize(String data) {
        LinkedList<String> converted = convert(data);

        return deserialize(converted);
    }

    private BinaryTreeNode<Integer> deserialize(LinkedList<String> data) {
        if (data.size() == 0) {
            return null;
        }

        String curr = data.removeFirst();
        if (curr.equals("null")) {
            return null;
        }
        BinaryTreeNode<Integer> node = new BinaryTreeNode<>(Integer.valueOf(curr));
        node.left = deserialize(data);
        node.right = deserialize(data);

        return node;
    }

    private LinkedList<String> convert(String data) {
        String[] arr = data.split(",");
        LinkedList<String> lst = new LinkedList<>();
        for (String a : arr) {
            lst.add(a);
        }
        return lst;
    }

    public static void main(String[] args) {
        SerializeDeserialize unit = new SerializeDeserialize();

        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1, new BinaryTreeNode<>(2), new BinaryTreeNode<>(3, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)));
        System.out.println(unit.serialize(root));

        preOrderAssert(unit.deserialize(unit.serialize(root)), root);
        preOrderAssert(unit.deserialize(unit.serialize(null)), null);

        BinaryTreeNode<Integer> root2 = new BinaryTreeNode<>(1);
        preOrderAssert(unit.deserialize(unit.serialize(root2)), root2);

        BinaryTreeNode<Integer> root3 = new BinaryTreeNode<>(1, null, new BinaryTreeNode<>(2, null, new BinaryTreeNode<>(3)));
        preOrderAssert(unit.deserialize(unit.serialize(root3)), root3);

        System.out.println(unit.serialize(root3));
    }

    private static void preOrderAssert(BinaryTreeNode<Integer> value, BinaryTreeNode<Integer> expected) {
        if (value == null) {
            if (expected != null) {
                System.out.println(expected.value);
            }
            assertNull(expected);
            return;
        }
        assertEquals(expected.value, value.value);
        preOrderAssert(value.left, expected.left);
        preOrderAssert(value.right, expected.right);
    }


}
