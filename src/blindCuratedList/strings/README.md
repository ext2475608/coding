[TOC]

# Questions

## Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

```
Example 1:

Input: s = "anagram", t = "nagaram"
Output: true

Example 2:

Input: s = "rat", t = "car"
Output: false

 

Constraints:

    1 <= s.length, t.length <= 5 * 104
    s and t consist of lowercase English letters.
```

We can simply keep the chars in a set (for `O(1)` lookup) for the first string. If all chars in the second string also exists in the set, its an anagram. The caveat is if the strings are of different sizes, they are definitely not anagrams. However, empty strings are anagrams, I guess.

Time complexity is `O(n)` since we are traversing the strings once. And space complexity is O(max(m, n)), where m and n are the sizes of the strings.

