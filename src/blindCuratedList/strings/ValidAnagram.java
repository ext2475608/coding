package blindCuratedList.strings;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

public class ValidAnagram {
    
    public boolean isValidAnagram(String firstString, String secondString) {
        if (firstString.length() != secondString.length()) {
            return false;
        }

        if (firstString.isEmpty() && secondString.isEmpty()) {
            return true;
        }

        // Put all chars in first string in a set
        Set<Character> store = new HashSet<Character>();
        for (char c : firstString.toCharArray()) {
            store.add(c);
        }

        for (char c : secondString.toCharArray()) {
            if (!store.contains(c)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        ValidAnagram unit = new ValidAnagram();
        assertEquals(true, unit.isValidAnagram("anagram", "nagaram"));
        assertEquals(false, unit.isValidAnagram("rat", "car"));
        assertEquals(false, unit.isValidAnagram("abcd", "abc"));
        assertEquals(true, unit.isValidAnagram("", ""));
    }

}
