package blindCuratedList.matrix;

import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {

    int rowOffset;
    int colOffset;

    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> result = new ArrayList<>();

        rowOffset = 0;
        colOffset = 1;
        traverse(matrix, 0, 0, result, new boolean[matrix.length][matrix[0].length]);

        return result;
    }

    private void traverse(int[][] matrix, int row, int col, List<Integer> result, boolean[][] visited) {

        if (row < 0 || col < 0 || row >= matrix.length || col >= matrix[0].length || visited[row][col]) {
            return;
        } else {
            visited[row][col] = true;
            result.add(matrix[row][col]);

            int nextRow = row + rowOffset;
            int nextCol = col + colOffset;

            if (nextRow >= 0 && nextCol >=0 && nextRow < matrix.length && nextCol < matrix[0].length) {
                if (!visited[nextRow][nextCol]) {
                    traverse(matrix, nextRow, nextCol, result, visited);
                } else {
                    changeDirection();
                    traverse(matrix, row + rowOffset, col + colOffset, result, visited);
                }
            } else {
                changeDirection();
                traverse(matrix, row + rowOffset, col + colOffset, result, visited);
            }  
        }

    }

    private void changeDirection() {
        if (rowOffset == 0 && colOffset == 1) {
            // was right, go down
            rowOffset = 1;
            colOffset = 0;
        } else if (rowOffset == 1 && colOffset == 0) {
            // was down, go left
            rowOffset = 0;
            colOffset = -1;
        } else if (rowOffset == 0 && colOffset == -1) {
            // left, go up
            rowOffset = -1;
            colOffset = 0;
        } else {
            // was up, go right
            rowOffset = 0;
            colOffset = 1;
        }
    }

    public static void main(String[] args) {
        SpiralMatrix unit = new SpiralMatrix();

        System.out.println(unit.spiralOrder(new int[][]{{1,2,3},{4,5,6},{7,8,9}}));
        System.out.println(unit.spiralOrder(new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}}));
    }

}
