package blindCuratedList.matrix;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WordSearch {

    public boolean exist(char[][] board, String word) {
        boolean exist = false;

        for (int i=0; i<board.length; i++) {
            for (int j=0; j<board[0].length; j++) {
                exist |= exist(board, word, 0, i, j);
            }
        }

        return exist;
    }

    private boolean exist(char[][] board, String word, int wordIdx, int row, int col) {
        if (row < 0 || col < 0 || row >= board.length || col >= board[0].length) {
            return false;
        }
        
        char curr = board[row][col];

        if (curr == '$') {
            return false;
        }

        if (curr == word.charAt(wordIdx) && wordIdx == word.length()-1) {
            return true;
        }

        if (curr != word.charAt(wordIdx)) {
            return false;
        }

        board[row][col] = '$';

        boolean exist = exist(board, word, wordIdx+1, row+1, col) || exist(board, word, wordIdx+1, row, col+1) || exist(board, word, wordIdx+1, row-1, col) || exist(board, word, wordIdx+1, row, col-1);

        board[row][col] = curr;

        return exist;
    }

    public static void main(String[] args) {
        WordSearch unit = new WordSearch();

        assertTrue(unit.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "ABCCED"));
        assertTrue(unit.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "SEE"));
        assertFalse(unit.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "ABCB"));
        assertTrue(unit.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "E"));
        assertFalse(unit.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "AFE"));
    }

}
