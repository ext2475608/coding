package blindCuratedList.matrix;

import common.PrinterUtils;

public class SetMatrixZeroes {

    public void setZeroes(int[][] matrix) {
        boolean isFirstColMarked = false;
        int rowLen = matrix.length;
        int colLen = matrix[0].length;
        for (int i=0; i<rowLen; ++i) {
            if (matrix[i][0] == 0) {
                // mark first col to true
                isFirstColMarked = true;
            }
            // start from 1 because first col is used as flag
            for (int j=1; j<colLen; ++j) {
                int curr = matrix[i][j];
                if (curr == 0) {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }
        // iterate and fill in the matrix
        // check from the second row and col, we will check for the first ones later
        for (int i=1; i<rowLen; ++i) {
            for (int j=1; j<colLen; ++j) {
                if (matrix[i][0] == 0 || matrix[0][j] == 0) {
                    matrix[i][j]=0;
                }
            }
        }
        // check for first row
        if (matrix[0][0] == 0) {
            // set entire row to 0
            for (int i=0; i<colLen; i++) {
                matrix[0][i]=0;
            }
        }
        // check for first col
        if (isFirstColMarked) {
            // set entire col to 0
            for (int i=0; i<rowLen; ++i) {
                matrix[i][0]=0;
            }
        }
    }

    public static void main(String[] args) {
        SetMatrixZeroes unit = new SetMatrixZeroes();
        int[][] matrix = new int[][]{{1,1,1},{1,0,1},{1,1,1}};
        unit.setZeroes(matrix);
        PrinterUtils.print(matrix);

        int[][] matrix2 = new int[][]{{1,0}};
        unit.setZeroes(matrix2);
        PrinterUtils.print(matrix2);

        int[][] matrix3 = new int[][]{{1},{0}};
        unit.setZeroes(matrix3);
        PrinterUtils.print(matrix3);
    }

}
