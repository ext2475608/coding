package basics.tree_graph_traversals;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import common.BinaryTreeNode;

public class DfsTraversals {
    
    // left - root - right
    public List<Integer> inorder(BinaryTreeNode<Integer> root) {
        List<Integer> results = new ArrayList<>();
        inOrder(root, results);
        return results;
    }

    private void inOrder(BinaryTreeNode<Integer> node, List<Integer> results) {
        if (node == null) {
            return;
        }

        inOrder(node.left, results);
        results.add(node.value);
        inOrder(node.right, results);
    }

    // root - left - right
    public List<Integer> preorder(BinaryTreeNode<Integer> root) {
        List<Integer> results = new ArrayList<>();
        preOrder(root, results);
        return results;
    }

    private void preOrder(BinaryTreeNode<Integer> node, List<Integer> results) {
        if (node == null) {
            return;
        }

        results.add(node.value);
        preOrder(node.left, results);
        preOrder(node.right, results);
    }

    // left - right - root
    public List<Integer> postorder(BinaryTreeNode<Integer> root) {
        List<Integer> results = new ArrayList<>();
        postOrder(root, results);
        return results;
    }

    private void postOrder(BinaryTreeNode<Integer> node, List<Integer> results) {
        if (node == null) {
            return;
        }

        postOrder(node.left, results);
        postOrder(node.right, results);
        results.add(node.value);
    }

    public static void main(String... args) {
        DfsTraversals unit = new DfsTraversals();

        /**
         *                  1
         *             2           3
         *          4       5
         */
        
        BinaryTreeNode<Integer> root =  new BinaryTreeNode<>(1, new BinaryTreeNode<>(2, new BinaryTreeNode<>(4), new BinaryTreeNode<>(5)), new BinaryTreeNode<>(3));
        
        assertEquals(List.of(4,2,5,1,3), unit.inorder(root));
        assertEquals(List.of(1,2,4,5,3), unit.preorder(root));
        assertEquals(List.of(4,5,2,3,1), unit.postorder(root));
    }

}
