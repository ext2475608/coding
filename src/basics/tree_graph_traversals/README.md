[TOC]

# Theory
## DFS based tree traversal methods
### Inorder
Direction of traversal: Left - Root - Right

### Preorder
Direction of traversal: Root - Left - Right

### Postorder
Direction of traversal: Left - Right - Root

**Implementation: [DfsTraversals](DfsTraversals.java)**