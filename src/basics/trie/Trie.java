package basics.trie;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import common.TrieNode;

public class Trie {

    TrieNode<Character> root;

    public Trie() {
        root = new TrieNode<>();
    }

    public void insert(String word) {
        TrieNode<Character> node = root;

        for (char curr : word.toCharArray()) {
            if (node.children.containsKey(curr)) {
                // if the trie contains the char, move to that char node.
                node = node.children.get(curr);
            } else {
                // if the trie doesnt contain the char, insert it and move to that node.
                node.children.put(curr, new TrieNode<>());
                node = node.children.get(curr);
            }
        }
        node.isEnd = true;
    }

    public boolean searchWord(String word) {
        TrieNode<Character> node = root;

        for (char curr : word.toCharArray()) {
            if (node.children.containsKey(curr)) {
                node = node.children.get(curr);
            } else {
                return false;
            }
        }

        return node.isEnd; // must check if this is a word end.
    }

    public boolean searchPrefix(String prefix) {
        TrieNode<Character> node = root;

        for (char curr : prefix.toCharArray()) {
            if (node.children.containsKey(curr)) {
                node = node.children.get(curr);
            } else {
                return false;
            }
        }

        return true;
    }

    public static void main(String... args) {
        Trie unit = new Trie();

        /**
         * trie.insert("apple");
            trie.search("apple");   // return True
            trie.search("app");     // return False
            trie.startsWith("app"); // return True
            trie.insert("app");
            trie.search("app");     // return True
         */
        unit.insert("apple");
        assertTrue(unit.searchWord("apple"));
        assertFalse(unit.searchWord("app"));
        assertTrue(unit.searchPrefix("app"));
        unit.insert("app");
        assertTrue(unit.searchWord("app"));
    }
    
}
