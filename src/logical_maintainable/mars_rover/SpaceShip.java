package logical_maintainable.mars_rover;

import java.util.LinkedList;

public class SpaceShip {
    Rover rover;

    public SpaceShip() {
        rover = new Rover();
    }

    public void acceptBatch(LinkedList<CardinalDirection> directions)  {
        for (CardinalDirection direction : directions) {
            boolean result = rover.move(direction);
            if (result) {
                System.out.println("Successfully moved " + direction);
            } else {
                System.out.println("Failed to move " + direction);
            }
        }
    }
}
