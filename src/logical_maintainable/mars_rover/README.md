## Design a Mars rover that will explore the surface of Mars. The requirements are:

1. The surface of Mars is defined as a flat 2D grid of cells like:

```
    0   1   2   3   4
0   

1           R

2

3

```

2. Initially, the rover would land at any valid cell on the surface. At any point in time, the rover can occupy one cell on the surface. The rover can move in any of the cardinal directions (North, South, East or West) by one cell at a time.

3. Since sending signals to the Rover from Earth is expensive, Mission Control has proposed to be able to send the moves in batches. The batches would be sent to a space ship orbiting Mars which would then beam the move down to the rover.

### Implementation

[CardinalDirection.java](CardinalDirection.java)

[Location.java](Location.java.java)

[MarsSurface.java](MarsSurface.java)

[MissionControl.java](MissionControl.java)

[Rover.java](Rover.java)

[SpaceShip.java](SpaceShip.java)