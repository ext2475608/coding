package logical_maintainable.mars_rover;

import java.util.*;

public class MissionControl {
    
  

    public static void main(String... args) {
        // This is mission control....
        SpaceShip spaceShip = new SpaceShip();
        LinkedList<CardinalDirection> inputList = new LinkedList<>();
        inputList.add(CardinalDirection.E);
        inputList.add(CardinalDirection.N);
        inputList.add(CardinalDirection.N);
        inputList.add(CardinalDirection.N);
        inputList.add(CardinalDirection.N);
        inputList.add(CardinalDirection.N);
        inputList.add(CardinalDirection.S);
        spaceShip.acceptBatch(inputList);

    }

}
