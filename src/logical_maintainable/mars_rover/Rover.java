package logical_maintainable.mars_rover;

import java.util.*;

public class Rover {
    MarsSurface marsSurface;

    static Map<CardinalDirection, int[]> moveMap = Map.of(
        CardinalDirection.N, new int[]{1,0},
        CardinalDirection.S, new int[]{-1,0},
        CardinalDirection.E, new int[]{0,1},
        CardinalDirection.W, new int[]{0,-1}
    );

    Location currentLocation;

    public Rover() {
        currentLocation = new Location(0, 0);
        marsSurface = new MarsSurface(0,4,0,5);
    }

    public boolean move(CardinalDirection direction) {
        if (!validateMove(direction)) {
            return false;
        }
        
        Location nextLocation = nextLocation(direction);
        this.currentLocation = nextLocation;

        return true;
    }

    private boolean validateMove(CardinalDirection direction) {
        Location nextLocation = nextLocation(direction);
        return marsSurface.isWithinSurface(nextLocation);
    }

    private Location nextLocation(CardinalDirection direction) {
        int[] moveCoord = moveMap.get(direction);
        return new Location(currentLocation.row + moveCoord[0], currentLocation.col + moveCoord[1]);
    }
}
