package logical_maintainable.mars_rover;

public enum CardinalDirection {
    N, S, E, W
}
