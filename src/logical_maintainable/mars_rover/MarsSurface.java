package logical_maintainable.mars_rover;

public class MarsSurface {
    public int maxRows;
    public int minRows;
    public int maxCols;
    public int minCols;

    public MarsSurface(int minRows, int maxRows, int minCols, int maxCols) {
        this.minRows = minRows;
        this.maxRows = maxRows;
        this.minCols = minCols;
        this.maxCols = maxCols;
    }

    public boolean isWithinSurface(Location location) {
        return location.row >= minRows && location.row <= maxRows && location.col >= minCols && location.col <= maxCols;
    }
}
