package leetcode.stacks.easy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class ValidParens {
    
    private static final Set<Character> OPEN_BRACKETS = Set.of('(', '{', '[');
    private static final Map<Character, Character> COMPLEMENTS = Map.of(')', '(', '}', '{', ']', '[');

    public boolean isValid(String input) {
        Stack<Character> stk = new Stack<>();

        for (char curr : input.toCharArray()) {
            if (OPEN_BRACKETS.contains(curr)) {
                stk.add(curr);
            } else {
                if (stk.isEmpty() || stk.peek() != COMPLEMENTS.get(curr)) {
                    return false;
                }
                stk.pop();
            }
        }

        return stk.size() == 0;
    }

    public static void main(String... args) {
        ValidParens unit = new ValidParens();

        assertTrue(unit.isValid("()"));
    }

}
