package leetcode.backtracking;

public class WordSearch {

    public boolean isPresent(char[][] matrix, String word) {
        boolean isPresent = false;
        for (int i=0; i<matrix.length; i++) {
            for (int j=0; j<matrix[0].length; j++) {
                if (matrix[i][j] == word.charAt(0)) {
                    isPresent |= isPresent(matrix, word, 0, i, j, new boolean[matrix.length][matrix[0].length]);
                }
            }
        }
        return isPresent;
    }

    private boolean isPresent(char[][] matrix, String word, int wordIdx, int i, int j, boolean[][] visited) {        
        if (wordIdx == word.length()-1) {
            // found the word
            return true;
        }

        if (i >= matrix.length || i < 0 || j >= matrix[0].length || j < 0) {
            return false;
        }

        if (visited[i][j]) {
            return false;
        }

        char currChar = matrix[i][j];
        visited[i][j] = true;
        boolean isPresent = false;
        if (currChar == word.charAt(wordIdx)) {
            isPresent = isPresent(matrix, word, wordIdx++, i+1, j, visited) || isPresent(matrix, word, wordIdx++, i, j+1, visited) || isPresent(matrix, word, wordIdx++, i-1, j, visited) || isPresent(matrix, word, wordIdx++, i, j-1, visited);
            wordIdx--;
            visited[i][j] = false;
        } else {
            isPresent = false;
        }
        
        return isPresent;
    } 

    public static void main(String... args) {
        WordSearch unit = new WordSearch();

        System.out.println(unit.isPresent(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "ABCCED"));
        System.out.println(unit.isPresent(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "ABCCEDJ"));
    }

}
