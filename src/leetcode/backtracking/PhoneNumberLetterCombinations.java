package leetcode.backtracking;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PhoneNumberLetterCombinations {

    Map<Character, String> phoneDigits = Map.of('2', "abc",
        '3', "def",
        '4', "ghi",
        '5', "jkl",
        '6', "mno",
        '7', "pqrs",
        '8', "tuv",
        '9', "wxyz");
    
    public List<String> combinations(String digits) {
        List<String> results = new ArrayList<>();
        combinations(digits, 0, results, new StringBuilder());
        return results;
    }

    private void combinations(String digits, int digitsIdx, List<String> results, StringBuilder current) {
        if (current.length() == digits.length()) {
            results.add(current.toString());
            return;
        }

        if (digitsIdx >= digits.length()) {
            return;
        }

        String letters = phoneDigits.get(digits.charAt(digitsIdx));
        for (char c : letters.toCharArray()) {
            current.append(c);
            combinations(digits, digitsIdx+1, results, current);
            current.deleteCharAt(current.length()-1);
        }
    }

    public static void main(String... args) {
        PhoneNumberLetterCombinations unit = new PhoneNumberLetterCombinations();

        assertEquals(List.of("ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"), unit.combinations("23"));
        assertEquals(List.of("a", "b", "c"), unit.combinations("2"));
    }

}
