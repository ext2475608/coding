package leetcode.backtracking;

import java.util.ArrayList;
import java.util.List;

public class GenerateParens {

    public List<String> generate(int n) {
        List<String> result = new ArrayList<>();
        generate(result, 0, 0, n, new StringBuilder());
        return result;
    }

    private void generate(List<String> result, int open, int close, int max, StringBuilder curr) {
        if (curr.length() == max * 2) { // max * 2 means there are a equal number of ( and )'s.
            result.add(curr.toString());
            return;
        }

        if (open < max) { // we add ('s as long as the total number is less than inputted n.
            curr.append('(');
            generate(result, open+1, close, max, curr);
            curr.deleteCharAt(curr.length()-1); // after it has generated the string, we remove the added (, so we can form other combinations.
        }

        if (close < open) { // we add )'s as long as the string is un-balanced and ) is less than (.
            curr.append(')');
            generate(result, open, close+1, max, curr);
            curr.deleteCharAt(curr.length()-1); // similarly, we remove the added ), to generate other combinations.
        }
    }

    public static void main(String... args) {
        GenerateParens unit = new GenerateParens();

        System.out.println(unit.generate(3));
        System.out.println(unit.generate(1));
    }

}
