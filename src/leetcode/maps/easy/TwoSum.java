package leetcode.maps.easy;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> mp = new HashMap<>();
        
        for (int i=0; i<nums.length; i++) {
            mp.put(nums[i], i);
        }
        
        for (int i=0; i<nums.length; i++) {
            int complement = target - nums[i];
            if (mp.containsKey(complement) && mp.get(complement) != i) {
                return new int[]{i, mp.get(complement)};
            }
        }
        
        return new int[]{-1,-1};
    }

    public static void main(String... args) {
        TwoSum unit = new TwoSum();

        assertTrue(Arrays.equals(new int[]{0,1}, unit.twoSum(new int[]{2,7,11,15}, 9)));
    }

}
