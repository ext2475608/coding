package common;

public class ListNode<E> {
    public E val;
    public ListNode<E> next;
    public ListNode<E> previous;

    public ListNode(E val) {
        this.val = val;
        this.next = null;
        this.previous = null;
    }

    public ListNode(E val, ListNode<E> next) {
        this.val = val;
        this.next = next;
        this.previous = null;
    }

    public ListNode(E val, ListNode<E> next, ListNode<E> previous) {
        this.val = val;
        this.next = next;
        this.previous = previous;
    }
}
