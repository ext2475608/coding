package common;

import java.util.*;

public class PrinterUtils {
    public static void print(int[] arr) {
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void print(int[][] arr) {
        for (int i=0; i<arr.length; i++) {
            for (int j=0; j<arr[0].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void print(List<int[]> lst) {
        lst.forEach(l -> {
            for (int i : l) {
                System.out.print(i + " ");
            }
            System.out.println();
        });
    }

    public static void print(BinaryTreeNode root) {
        if (root == null) {
            System.out.println("null");
        }

        Queue<BinaryTreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int levelCnt = 1;

        while (!queue.isEmpty()) {
            int cnt = 0;
            for (int i=0; i<levelCnt; i++) {
                BinaryTreeNode curr = queue.poll();
                System.out.print(curr.value + " (next:" + (curr.next != null ? curr.next.value : "null") + ") \t");
                if (curr.left != null) {
                    queue.offer(curr.left);
                    cnt++;
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                    cnt++;
                }
            }
            System.out.println();
            levelCnt = cnt;
        }
    }

    public static void print(ListNode head) {
        ListNode temp = head;
        while (temp != null) {
            System.out.print(temp.val + "->");
            temp = temp.next;
        }
        System.out.print("null\n");
    }

}
