package common;

public class Pair<U,T> {
    
    public U first;
    public T second;

    public Pair(U u, T t) {
        this.first = u;
        this.second = t;
    }

    @Override
    public boolean equals(Object o) {
        if (! (o instanceof Pair) ) {
            return false;
        }

        Pair p = (Pair) o;

        return p.first.equals(this.first) && p.second.equals(this.second);
    }

    @Override
    public String toString() {
        return "first=" + String.valueOf(first) + ", second=" + String.valueOf(second);
    }

}
