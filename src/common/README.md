# Common datastructures

1. [BinaryTreeNode](BinaryTreeNode.java)
2. [ListNode](ListNode.java)
3. [Pair or Tuple](Pair.java)
4. [2D coord or Point](Point.java)
5. [TrieNode](TrieNode.java)

