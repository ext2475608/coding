package common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.function.Function;

public class Profiler {
    private Instant start;
    private Instant end;
    private Class testSuiteName;
    private String methodName;
    
    protected void startTimer(Class testSuiteName) {
        this.start = Instant.now();
        this.testSuiteName = testSuiteName;
    }

    protected void startTimer(Class testSuiteName, String methodName) {
        this.start = Instant.now();
        this.testSuiteName = testSuiteName;
        this.methodName = methodName;
    }

    protected void endTimer() {
        end = Instant.now();

        outputTime();
    }

    public synchronized void recordTime(Class clazz) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        String[] params = null;

        startTimer(clazz);
        clazz.getDeclaredMethod("main", String[].class).invoke(null, (Object)params);
        endTimer();
    }

    private void outputTime() {
        String message = testSuiteName.getName()
             + "," + ChronoUnit.MICROS.between(start, end) + "," + ChronoUnit.MILLIS.between(start, end)
             + "," + Runtime.getRuntime().totalMemory()
             + "," + Runtime.getRuntime().freeMemory();

        if (ProfilerLogger.PROFILER_WRITER != null) {
            try {
                System.out.println("Writing message " + message);
                ProfilerLogger.PROFILER_WRITER.write(message + "\n");
            } catch (Exception e) {
                System.out.println("Couldnt write timer message " + e.getMessage());
            }
        }
    }
}
