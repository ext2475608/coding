package common;

import java.util.HashMap;
import java.util.Map;

public class TrieNode<T> {
    public boolean isEnd;
    public Map<T, TrieNode<T>> children;

    public TrieNode(boolean isEnd, Map<T, TrieNode<T>> children) {
        this.isEnd = isEnd;
        this.children = children;
    }

    public TrieNode() {
        children = new HashMap<>();
        isEnd = false;
    }
}
