package common;

public class BinaryTreeNode<E> {
    public E value;

    public BinaryTreeNode<E> left;

    public BinaryTreeNode<E> right;

    public BinaryTreeNode<E> next;

    public BinaryTreeNode() { }

    public BinaryTreeNode(E value, BinaryTreeNode<E> left, BinaryTreeNode<E> right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public BinaryTreeNode(E value, BinaryTreeNode<E> left, BinaryTreeNode<E> right, BinaryTreeNode<E> next) {
        this.value = value;
        this.left = left;
        this.right = right;
        this.next = next;
    }

    public BinaryTreeNode(E value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("value=%s, left=%s, right=%s", this.value, this.left, this.right);
    }
}
