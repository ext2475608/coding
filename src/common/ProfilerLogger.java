package common;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class ProfilerLogger {
    public static BufferedWriter PROFILER_WRITER;

    public static void createWriter() {
        try {
            PROFILER_WRITER = new BufferedWriter(new FileWriter("timer.csv"));
            PROFILER_WRITER.write("Class,Time(micros),Time(millis),Heap Mem,Heap Free\n");
        } catch (Exception e) {
            System.out.println("Cannot create timer file. Profile times wont be logged.");
            System.out.println(e.getMessage());
        }
    }
}
