package misc;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

public class TwoSumPairs {

    public int count(int[] nums, int target) {
        if (nums.length < 2) {
            return 0;
        }

        Arrays.sort(nums);

        int low = 0, high = nums.length - 1, count = 0;;
        while (low < high) {
            int sum = nums[low] + nums[high];
            if (sum <= target) {
                count += high - low;
                low++;
            } else {
                high--;
            }
        }

        return count;
    }

    public static void main(String... args) {
        TwoSumPairs unit = new TwoSumPairs();

        assertEquals(2, unit.count(new int[]{2, 7, 11, 15}, 13));
        assertEquals(1, unit.count(new int[]{2, 7, 11, 15}, 9));
        assertEquals(2, unit.count(new int[]{2, 7, 11, 15}, 14));
        assertEquals(0, unit.count(new int[]{2, 7}, 5));
    }

}
