package misc;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

public class UniquePairs {

    public int findPairs(int[] nums, int target) {
        if (nums.length < 2) {
            return 0;
        }

        int low = 0, high = nums.length-1, count = 0;
        Arrays.sort(nums);
        while (low < high) {
            int sum = nums[low] + nums[high];
            if (sum == target) {
                count++;
                low++;
                high--;

                while (low < high && nums[low] == nums[low-1]) {
                    low++;
                }
                while (low < high && nums[high] == nums[high+1]) {
                    high--;
                }
            } else if (sum > target) {
                high--;
            } else {
                low++;
            }
        }

        return count;
    }

    public static void main(String... args) {
        UniquePairs unit = new UniquePairs();

        assertEquals(2, unit.findPairs(new int[]{1, 1, 2, 45, 46, 46}, 47));
    }

}
