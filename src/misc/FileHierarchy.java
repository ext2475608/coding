package misc;

import java.util.List;
import java.util.Map;

import common.TrieNode;

public class FileHierarchy {
    
    /**
     * Given a list of file paths, print the file hierarchy

    I/P:
    file_paths: [
    "/users/home/html/a.html",
    "/users/home/html/b.html",
    "/users/home/index.html",
    "/users/home/js/a.js"
    ]

    O/P:
    users
        home
            html
            index.html
                a.html
                b.html
            js
                a.js
    

        users
                    home
        index.html       html       js
            a.html  b.html       a.js
     */

    
    private TrieNode<String> root;

    public FileHierarchy(List<String> paths) {
        root = new TrieNode<>();
        populatePaths(paths);
    }


    private void populatePaths(List<String> paths) {
        for (String path : paths) {
            TrieNode<String> node = root;

            String[] fileOrFolderArr = path.split("/");
            for (String fileOrFolder : fileOrFolderArr) {
                if (node.children.containsKey(fileOrFolder)) {
                    node = node.children.get(fileOrFolder);
                } else {
                    node.children.put(fileOrFolder, new TrieNode<>());
                    node = node.children.get(fileOrFolder);
                }
            }
            node.isEnd = true;
        }
    }

    public void printPaths() {
        TrieNode<String> node = root;

        printPaths(node.children, 0);
    }

    private void printPaths(Map<String, TrieNode<String>> children, int tabCnt) {
        children.entrySet().forEach(entry -> {
            for (int i=0; i<tabCnt; i++) {System.out.print("\t");}
            System.out.print(entry.getKey());
            System.out.println("");
            printPaths(entry.getValue().children, tabCnt+1);
        });
    }

    public static void main(String... args) {
        FileHierarchy unit = new FileHierarchy(List.of("/users/home/html/a.html",
        "/users/home/html/b.html",
        "/users/home/index.html",
        "/users/home/js/a.js"));

        unit.printPaths();
    }


}
