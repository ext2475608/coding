package misc;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import common.PrinterUtils;


public class TwoSum {
    
    public int[] noDuplicates(int[] nums, int target) {
        if (nums.length < 2) {
            return new int[]{};
        }

        Map<Integer, Integer> store = new HashMap<>();
        for (int i=0; i<nums.length; i++) {
            store.put(nums[i], i);
        }

        int idx = 0;
        for (int num : nums) {
            int compliment = target - num;
            if (store.containsKey(compliment) && store.get(compliment) != idx) {
                return new int[]{idx, store.get(compliment)};
            }
            idx++;
        }

        return new int[]{-1,-1};
    }

    public int[] duplicates(int[] nums, int target) {
        if (nums.length < 2) {
            return new int[]{};
        }

        Map<Integer, Set<Integer>> store = populateStore(nums);

        for (int i=0; i<nums.length; i++) {
            int compliment = target - nums[i];
            if (store.containsKey(compliment) && checkDuplicate(compliment, i, store.get(compliment)) ) {
                return new int[]{nums[i], compliment};
            }
        }

        return new int[]{-1,-1};
    }

    public int[] sorted(int[] nums, int target) {
        if (nums.length < 2) {
            return new int[]{};
        }

        int low = 0, high = nums.length-1;
        while (low < high) { // shouldnt be =, we dont want to re-use an index.
            int sum = nums[low] + nums[high];
            if (sum > target) {
                high = high-1;
            } else if (sum == target) {
                return new int[]{low, high};
            } else {
                low = low+1;
            }
        }

        return new int[]{-1,-1};
    }

    public static void main(String... args) {
        TwoSum unit = new TwoSum();

        assertTrue(Arrays.equals(unit.noDuplicates(new int[]{2, 7, 11, 15}, 9), new int[]{0,1}));
        assertTrue(Arrays.equals(unit.noDuplicates(new int[]{2, 7, 11, 15}, 14), new int[]{-1,-1}));
        assertTrue(Arrays.equals(unit.noDuplicates(new int[]{}, 9), new int[]{}));
        assertTrue(Arrays.equals(unit.noDuplicates(new int[]{1}, 9), new int[]{}));
        assertTrue(Arrays.equals(unit.noDuplicates(new int[]{1,2}, 3), new int[]{0,1}));
        assertTrue(Arrays.equals(unit.noDuplicates(new int[]{1,2}, 4), new int[]{-1,-1}));

        assertTrue(Arrays.equals(unit.duplicates(new int[]{2, 7, 11, 15}, 9), new int[]{2,7}));
        assertTrue(Arrays.equals(unit.duplicates(new int[]{2, 7, 11, 15}, 14), new int[]{-1,-1}));
        assertTrue(Arrays.equals(unit.duplicates(new int[]{}, 9), new int[]{}));
        assertTrue(Arrays.equals(unit.duplicates(new int[]{1}, 9), new int[]{}));
        assertTrue(Arrays.equals(unit.duplicates(new int[]{1,2}, 3), new int[]{1,2}));
        assertTrue(Arrays.equals(unit.duplicates(new int[]{1,2}, 4), new int[]{-1,-1}));
        assertTrue(Arrays.equals(unit.duplicates(new int[]{1,2,2}, 4), new int[]{2,2}));

        assertTrue(Arrays.equals(unit.sorted(new int[]{2, 7, 11, 15}, 9), new int[]{0,1}));
        assertTrue(Arrays.equals(unit.sorted(new int[]{2, 7, 11, 15}, 14), new int[]{-1,-1}));
        assertTrue(Arrays.equals(unit.sorted(new int[]{}, 9), new int[]{}));
        assertTrue(Arrays.equals(unit.sorted(new int[]{1}, 9), new int[]{}));
        assertTrue(Arrays.equals(unit.sorted(new int[]{1,2}, 3), new int[]{0,1}));
        assertTrue(Arrays.equals(unit.sorted(new int[]{1,2}, 4), new int[]{-1,-1}));
        assertTrue(Arrays.equals(unit.sorted(new int[]{1,2,2}, 4), new int[]{1,2}));
    }

    private boolean checkDuplicate(int compliment, int index, Set<Integer> store) {
        if (store.contains(index)) {
            return store.size() > 1;
        } else {
            return store.size() > 0;
        }
    }

    private Map<Integer, Set<Integer>> populateStore(int[] nums) {
        Map<Integer, Set<Integer>> store = new HashMap<>();
        for (int i=0; i<nums.length; i++) {
            Set<Integer> set = store.getOrDefault(nums[i], new HashSet<>());
            set.add(i);
            store.put(nums[i], set);
        }
        return store;
    }

}
