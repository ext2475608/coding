package misc;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

public class ClosestSumToTarget {
    
    public int closestSum(int[] nums, int target) {
        if (nums.length < 2) {
            return -1;
        }

        Arrays.sort(nums);

        int low = 0, high = nums.length-1, bestSum = nums[0] + nums[1]; // dont init bestSum to 0 or something else, init to a random pair to avoid comparison issues down the line....
        while (low < high) {
            int sum = nums[low] + nums[high];
            if (Math.abs(target-sum) < Math.abs(target-bestSum)) {
                bestSum = sum;
            }
            if (sum > target) {
                high--;
            } else {
                low++;
            }
        }

        return bestSum;
    }

    public static void main(String... args) {
        ClosestSumToTarget unit = new ClosestSumToTarget();

        assertEquals(13, unit.closestSum(new int[]{2, 7, 11, 15}, 13));
        assertEquals(13, unit.closestSum(new int[]{2, 7, 11, 15}, 14));
        assertEquals(9, unit.closestSum(new int[]{2, 7, 11, 15}, 11));
        assertEquals(9, unit.closestSum(new int[]{2, 7, 11, 15}, 0));
        assertEquals(9, unit.closestSum(new int[]{2, 7, 11, 15}, -10));
    }


}
