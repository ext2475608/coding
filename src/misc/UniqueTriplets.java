package misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UniqueTriplets {
    
    public List<List<Integer>> triplets(int[] nums) {
        if (nums.length < 3) {
            return new ArrayList<>();
        }

        Arrays.sort(nums);

        List<List<Integer>> results = new ArrayList<>();

        for (int low = 0; low < nums.length-2; low++) {
            if (low > 0 && nums[low-1] == nums[low]) {
                continue;
            }
            List<List<Integer>> triplets = findPairs(nums, low); // we can start traversing from the current index because if there were a triplet that had a number before this index, it would have been captured by an iteration that started at an index before it.
            if (triplets.size() > 0) {
                results.addAll(triplets);
            }
        }

        return results;
    }

    private List<List<Integer>> findPairs(int[] nums, int low) {
        int high = nums.length-1;
        int num = nums[low++];

        List<List<Integer>> results = new ArrayList<>();

        while (low < high) {
            int sum = num + nums[low] + nums[high];
            if (sum == 0) {
                results.add(List.of(num, nums[low], nums[high]));

                low++;
                high--;

                while (low < high && nums[low] == nums[low-1]) {
                    low++;
                }
                while (low < high && nums[high] == nums[high+1]) {
                    high--;
                }
            } else if (sum > 0) {
                high--;
            } else {
                low++;
            }
        }

        return results;
    }


    public static void main(String... args) {
        UniqueTriplets unit = new UniqueTriplets();

        System.out.println(unit.triplets(new int[]{-1,-1,0,1,2}));
        System.out.println(unit.triplets(new int[]{-1,-1,0,1,1,2}));
    }

}
