package misc;

import java.util.*;

public class Rectangle {
    public int top;
    public int bottom;
    public int left;
    public int right;

    public Rectangle(int top, int bottom, int left, int right) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }

    public int width() {
        return Math.abs(this.left - this.right);
    }

    public int height() {
        return Math.abs(this.top - this.bottom);
    }

    public Rectangle clone() {
        return new Rectangle(this.top, this.bottom, this.left, this.right);
    }

    public Rectangle setRectangle(int top, int bottom, int left, int right) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;

        return this;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Rectangle)) {
            throw new IllegalArgumentException("Cannot compare with non-rectange");
        }

        Rectangle compare = (Rectangle)other;

        return this.top == compare.top &&
            this.bottom == compare.bottom &&
            this.left == compare.left &&
            this.right == compare.right;
    }

    @Override
    public String toString() {
        return "[" + this.top + "," + this.bottom + "," + this.left + "," + this.right + "]";
    }

    /** ==== Some operations ==== **/

    /**
     * Restrict the rectangle to the area of the intersection of the two rectangles.
     * Logic image: static/intersect.jpg
     * @param other - the other rectangle.
     * @return the restricted Rectangle.
     */
    public Rectangle restrictTo(Rectangle other) {
        if (this.isEmpty() || other.isEmpty()) {
            // no intersect area if one of the rectangles is empty.
            return this.setRectangle(0, 0, 0, 0);
        }

        int y1 = Math.max(this.left, other.left);
        int x1 = Math.max(this.bottom, other.bottom);
        int y2 = Math.min(this.right, other.right);
        int x2 = Math.min(this.top, other.top);

        return new Rectangle(x2, x1, y1, y2);
    }

    /**
     * Find the intersection between two rectangles.
     * @param other
     * @return
     */
    public Rectangle intersect(Rectangle other) {
        return this.clone().restrictTo(other);
    }


    /**
     * Subtracts (this-other). Per set theory, this should return all possible rectangles for the region covered by this that is NOT covered by other.
     * 
     * A simple case could be this: static/subtract-1.jpg.
     * 
     * But which usecase(s) covers all possibilities? - there could be a couple:
     * 1. If there's no overlap, then the answer is this.
     * 2. If there's overlap and other is contained within this, then there would be 4 different rectangles in the solution. This would be the max overlap we can get. Every 
     * other use-case would be a derivative of this.
     * 
     * Image: static/subtract-2.JPG (A is this, B is other).
     * @param other
     * @return
     */
    public List<Rectangle> subtract(Rectangle other) {
        Rectangle intersect = this.intersect(other);
        if (intersect.isEmpty()) {
            // rectangles do not overlap, return this
            return List.of(this.clone());
        }
        
        List<Rectangle> results = new ArrayList<>();

        // creating a left slice (r1)
        Rectangle leftSlice = new Rectangle(this.top, this.bottom, this.left, other.left);
        if (!leftSlice.isEmpty() && this.isWithin(leftSlice)) {
            results.add(leftSlice);
        }

        // creating a right slice (r2)
        Rectangle rightSlice = new Rectangle(this.top, this.bottom, other.right, this.right);
        if (!rightSlice.isEmpty() && this.isWithin(rightSlice)) {
            results.add(rightSlice);
        }

        // creating a top slice (r3)
        Rectangle topSlice = new Rectangle(this.top, other.top, other.left, Math.min(this.right, other.right));
        if (!topSlice.isEmpty() && this.isWithin(topSlice)) {
            results.add(topSlice);
        }

        // creating a bottom slice (r4)
        Rectangle bottomSlice = new Rectangle(other.bottom, this.bottom, other.left, other.right);
        if (!topSlice.isEmpty() && this.isWithin(bottomSlice)) {
            results.add(bottomSlice);
        }

        return results;
    }

    public List<Rectangle> union(Rectangle other) {
        List<Rectangle> subtract = this.subtract(other);
        if (subtract.isEmpty()) {
            // rectangles don't overlap, union must contain both rectangles because disjoint unions can contain repeated elements in both sets.
            return List.of(this.clone(), other.clone());
        }

        // if there's an overlap, we need to exclude the intersection.
        Rectangle intersect = this.intersect(other);
        
        // now the union will contain the region in this that's outside of the intersection.
        List<Rectangle> results = this.subtract(intersect);

        // also it will contain other.
        results.add(other.clone());

        return results;
    }

    /**
     * If the top is >= bottom or left >= right, then the rectangle cannot be formed, hence, is empty.
     * @return
     */
    public boolean isEmpty() {
        return this.top <= this.bottom || this.left >= this.right;
    }

    /**
     * Check if other is within the bounds of this.
     * @param other
     * @return
     */
    public boolean isWithin(Rectangle other) {
        return (other.top >= this.bottom && other.top <= this.top) &&
            (other.bottom >= this.bottom && other.bottom <= this.top) &&
            (other.left >= this.left && other.left <= this.right) &&
            (other.right >= this.left && other.right <= this.right);
    }


    /** ==== Tests ==== **/
    public static void main(String... args) {
        // Donut (case #2)
        Rectangle rectangle = new Rectangle(60, 20, 10, 50);
        Rectangle rectangle2 = new Rectangle(40, 30, 20, 40);

        System.out.println(rectangle.subtract(rectangle2));
        System.out.println(rectangle.union(rectangle2));

        rectangle = new Rectangle(60, 20, 10, 50);
        rectangle2 = new Rectangle(40, 10, 30, 90);

        System.out.println(rectangle.subtract(rectangle2));
        System.out.println(rectangle.union(rectangle2));
    }
}
