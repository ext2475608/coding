package misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UniqueQuadruplets {

    public List<List<Integer>> quadruplets(int[] nums, int target) {
        if (nums.length < 4) {
            return new ArrayList<>();
        }
        
        List<List<Integer>> results = new ArrayList<>();

        Arrays.sort(nums);

        for (int i=0; i<nums.length-3; i++) {
            if (i > 0 && nums[i-1] == nums[i]) {
                continue;
            }

            int n1 = nums[i];

            for (int j=i+1; j<nums.length-2; j++) {
                int n2 = nums[j];

                if (j > i+1 && nums[j] == nums[j-1]) {
                    continue;
                }

                int low = j+1, high = nums.length-1; 
                while (low < high) {
                    int sum = n1 + n2 + nums[low] + nums[high];

                    if (sum == target) {
                        results.add(List.of(n1, n2, nums[low], nums[high]));

                        low++;
                        high--;

                        while (low < high && nums[low] == nums[low-1]) {
                            low++;
                        }
                        while (low < high && nums[high] == nums[high+1]) {
                            high--;
                        }
                    } else if (sum > target) {
                        high--;
                    } else {
                        low++;
                    }
                }

            }
        }

        return results;
    }

    public static void main(String... args) {
        UniqueQuadruplets unit = new UniqueQuadruplets();

        System.out.println(unit.quadruplets(new int[]{-1,-1,1,1}, 0));
        System.out.println(unit.quadruplets(new int[]{-1,-1,1,2}, 0));
        System.out.println(unit.quadruplets(new int[]{-1,-1,1,1,1,2,-2,0,0}, 0));
    }

}
