package misc;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

public class PairTuples {
    
    public int count(int[] A, int[] B, int[] C, int[] D) {
        int count = 0;

        Map<Integer, Integer> sumFreqMap = new HashMap<>();

        for (int num1 : A) {
            for (int num2 : B) {
                int sum = num1 + num2;
                sumFreqMap.put(sum, sumFreqMap.getOrDefault(sum, 0)+1);
            }
        }

        for (int num3 : C) {
            for (int num4 : D) {
                int sum = -(num3 + num4);
                if (sumFreqMap.containsKey(sum)) {
                    count += sumFreqMap.get(sum);
                }
            }
        }

        return count;
    }

    public static void main(String... args) {
        PairTuples unit = new PairTuples();

        assertEquals(2, unit.count(new int[]{1, 2}, new int[]{-2,-1}, new int[]{-1, 2}, new int[]{0, 2}));
    }

}
